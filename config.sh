#!/bin/bash

TOP=$1
PREFIX=`echo $2 | sed "s/\\//\\\\\\\\\\\//g"`
HAVE_PGSQL=$3
HAVE_MYSQL=$4
HAVE_MONETDB=$5
HAVE_FBSQL=$6

if [ "$HAVE_MYSQL" == "yes" ]; then
    MYSQL_INCLUDE_DIR=`mysql_config --include | sed "s/\\//\\\\\\\\\\\//g"`
    MYSQL_LIBS=`mysql_config --libs | sed "s/\\//\\\\\\\\\\\//g"`
    sed -e "s/@prefix/$PREFIX/g" -e "s/@mysql_libs/$MYSQL_LIBS/g" -e "s/@mysql_include_dir/$MYSQL_INCLUDE_DIR/g" $TOP/src/mysql/libmycli.pc.in > $TOP/src/mysql/libmycli.pc
fi

if [ "$HAVE_PGSQL" == "yes" ]; then
    PG_INCLUDE_DIR=`pg_config --includedir | sed "s/\\//\\\\\\\\\\\//g"`
    PG_LIB_DIR=`pg_config --libdir | sed "s/\\//\\\\\\\\\\\//g"`
    sed -e "s/@prefix/$PREFIX/g" -e "s/@pg_include_dir/$PG_INCLUDE_DIR/g" -e "s/@pg_lib_dir/$PG_LIB_DIR/g" $TOP/src/pg/libpgcli.pc.in > $TOP/src/pg/libpgcli.pc
fi

if [ "$HAVE_MONETDB" == "yes" ]; then
    MAPI_INCLUDE=`pkg-config --cflags monetdb-mapi | sed "s/\\//\\\\\\\\\\\//g"`
    MAPI_LIBS=`pkg-config --libs monetdb-mapi | sed "s/\\//\\\\\\\\\\\//g"`
    sed -e "s/@prefix/$PREFIX/g" -e "s/@mapi_include/$MAPI_INCLUDE/g" -e "s/@mapi_libs/$MAPI_LIBS/g" $TOP/src/mdb/libmdbcli.pc.in > $TOP/src/mdb/libmdbcli.pc
fi

if [ "$HAVE_FBSQL" == "yes" ]; then
    GDS_LIBS=`fb_config --libs | sed "s/\\//\\\\\\\\\\\//g"`
    sed -e "s/@prefix/$PREFIX/g" -e "s/@gds_libs/$GDS_LIBS/g" $TOP/src/gds/libgdscli.pc.in > $TOP/src/gds/libgdscli.pc
fi
