#ifndef __LIBDBCLI_H__
#define __LIBDBCLI_H__

#include <libex/str.h>
#include <libex/net.h>
#include <libex/list.h>

#define DBCLI_STATE_LEN 16
#define DBCLI_MSG_LEN 256

extern __thread long dbcli_code;
extern __thread char dbcli_state [];
extern __thread char dbcli_msg [];

typedef struct {
    char *s_host;
    char *s_port;
    char *s_user;
    char *s_pass;
    char *s_dbname;
    int port;
} conn_info_t;
int parse_conn_info (const char *conn_info, conn_info_t *info);
void clear_conn_info (conn_info_t *info);

typedef struct dbpool dbpool_t;
typedef struct {
    dbpool_t *pool;
    time_t tm_released;
    list_item_t *li;
    int status;
} dbconn_t;

typedef enum {
    DBPOOL_ERROR,
    DBPOOL_CONNECT,
    DBPOOL_DISCONNECT,
    DBPOOL_LIVINGTIME,
    DBPOOL_MAXLEN,
} dbpool_opt_t;

typedef void (*dbpool_h) (void*);
typedef void *(*dbpool_connect_h) (const char*);
struct dbpool {
    char *conn_info;
    time_t livingtime;
    list_t *pg_free;
    list_t *pg_busy;
    dbpool_h on_error;
    dbpool_connect_h on_connect;
    dbpool_h on_disconnect;
    pthread_mutex_t locker;
    pthread_cond_t cond;
    pthread_condattr_t cond_attr;
    pthread_t th;
    int is_alive;
    int maxlen;
};

void dbpool_setopt_hnd (dbpool_t *pool, dbpool_opt_t opt, dbpool_h arg);
void dbpool_setopt_str (dbpool_t *pool, dbpool_opt_t opt, const char *arg);
void dbpool_setopt_int (dbpool_t *pool, dbpool_opt_t opt, long long int arg);
void dbpool_setopt_con (dbpool_t *pool, dbpool_opt_t opt, dbpool_connect_h arg);
#define dbpool_setopt(pool,opt,arg) \
    _Generic((arg), \
    dbpool_h: dbpool_setopt_hnd, \
    dbpool_connect_h: dbpool_setopt_con, \
    const char*: dbpool_setopt_str, \
    default: dbpool_setopt_int \
)(pool,opt,arg)

dbpool_t *dbpool_create ();
int dbpool_start (dbpool_t *pool);
void dbpool_free (dbpool_t *pool);
dbconn_t *dbpool_get (dbpool_t *pool);
void dbpool_release (dbconn_t *conn);

#endif
