/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __LIBGDSCLI_H__
#define __LIBGDSCLI_H__

#include <stdlib.h>
#include <stdarg.h>
#include <libex/str.h>
#include <ibase.h>
#include "dbcli.h"

void gds_error (ISC_STATUS_ARRAY status);

typedef struct gdsconn gdsconn_t;
typedef struct gdsres gdsres_t;
typedef struct gdsfld gdsfld_t;
typedef isc_tr_handle gdstr_t;

struct gdsconn {
    dbpool_t *pool;
    time_t tm_released;
    list_item_t *li;
    int status;
    isc_db_handle db;
};
gdsconn_t *gds_connect (const char *conn_info);
void gds_disconnect (gdsconn_t *conn);

extern char gds_concurrency [];
extern char gds_read_committed [];
#define GDS_TRAN_DEFAULT NULL, 0
#define GDS_TRAN_CONCURRENCY gds_concurrency, sizeof(gds_concurrency)
#define GDS_TRAN_READ_COMMITTED gds_read_committed, sizeof(gds_read_committed)

gdstr_t gds_start (char *tpb, int tpb_len, gdsconn_t *conn, ...);
static inline int gds_commit (gdstr_t *tr) {
    ISC_STATUS_ARRAY status;
    if (0 == isc_commit_transaction(status, tr))
        return 0;
    gds_error(status);
    return -1;
}
static inline int gds_rollback (gdstr_t *tr) {
    ISC_STATUS_ARRAY status;
    if (0 == isc_rollback_transaction(status, tr))
        return 0;
    gds_error(status);
    return -1;
}

struct gdsres {
    gdsconn_t *conn;
    isc_stmt_handle stmt;
    int status;
    XSQLDA *in_sqlda;
    XSQLDA *out_sqlda;
    short *out_ind;
    char **in_vars;
    int is_auto;
    isc_tr_handle tr;
};
gdsres_t *gds_prepare (gdsconn_t *conn, gdstr_t *tr, const char *sql, size_t sql_len);
int gds_exec (gdsres_t *res, gdstr_t *tr, gdsfld_t arg, ...);
int gds_exec_sql (gdsconn_t *conn, gdstr_t *tr, const char *sql, size_t sql_len);
int gds_fetch (gdsres_t *res);
inline static int gds_nflds (gdsres_t *res) {
    return res->out_sqlda ? res->out_sqlda->sqld : 0;
}
inline static int gds_fldlen (gdsres_t *res, int col) {
    return res->out_sqlda->sqlvar[col].sqllen;
};
short gds_type (gdsres_t *res, int col);
void gds_close (gdsres_t *res);

typedef void (*gds_setfld_h) (gdsres_t*, int, gdsfld_t*);

typedef union {
    ISC_UCHAR     b;
    ISC_SHORT     i2;
    ISC_LONG      i4;
    ISC_INT64     i8;
    char          *s;
    float         f4;
    double        f8;
    ISC_DATE      dt;
    ISC_TIME      tm;
    ISC_TIMESTAMP ts;
} gdsval_t;

struct gdsfld {
    gdsval_t val;
    short ind;
    int len;
    gds_setfld_h setfld;
};

void gds_set_bool (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_i2 (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_i4 (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_i8 (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_str (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_f4 (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_f8 (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_dt (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_tm (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_ts (gdsres_t *res, int col, gdsfld_t *fld);
void gds_set_null (gdsres_t *res, int col, gdsfld_t *fld);

#define GDS_TRUE (gdsfld_t){ .val = { .b = FB_TRUE }, .ind = 0, .setfld = gds_set_bool }
#define GDS_FALSE (gdsfld_t){ .val = { .b = FB_FALSE }, .ind = 0, .setfld = gds_set_bool }
#define GDS_I2(x) (gdsfld_t){ .val = { .i2 = x }, .ind = 0, .setfld = gds_set_i2 }
#define GDS_I4(x) (gdsfld_t){ .val = { .i4 = x }, .ind = 0, .setfld = gds_set_i4 }
#define GDS_I8(x) (gdsfld_t){ .val = { .i8 = x }, .ind = 0, .setfld = gds_set_i8 }
#define GDS_STR(x,l) (gdsfld_t){ .val = { .s = x }, .len = l, .ind = 0, .setfld = gds_set_str }
#define GDS_TXT(x,l) (gdsfld_t){ .val = { .s = x }, .len = l, .ind = 0, .setfld = gds_set_str }
#define GDS_F4(x) (gdsfld_t){ .val = { .f4 = x }, .ind = 0, .setfld = gds_set_f4 }
#define GDS_F8(x) (gdsfld_t){ .val = { .f8 = x }, .ind = 0, .setfld = gds_set_f8 }
#define GDS_DATE(x) (gdsfld_t){ .val = { .dt = x }, .ind = 0, .setfld = gds_set_dt }
#define GDS_TIME(x) (gdsfld_t){ .val = { .tm = x }, .ind = 0, .setfld = gds_set_tm }
#define GDS_TIMESTAMP(x) (gdsfld_t){ .val = { .ts = x }, .ind = 0, .setfld = gds_set_ts }
#define GDS_NULL (gdsfld_t){ .ind = 1, .setfld = gds_set_null }
#define GDS_END (gdsfld_t){ .setfld = NULL }

static inline int gds_is_null (gdsres_t *res, int col) {
    return SQL_NULL == res->out_sqlda->sqlvar[col].sqltype ? 1 : 0;
}

static inline short gds_get_i2 (gdsres_t *res, int col) {
    return *((ISC_SHORT*)res->out_sqlda->sqlvar[col].sqldata);
};

static inline int gds_get_i4 (gdsres_t *res, int col) {
    return *((ISC_LONG*)res->out_sqlda->sqlvar[col].sqldata);
};

static inline long long int gds_get_i8 (gdsres_t *res, int col) {
    return *((ISC_INT64*)res->out_sqlda->sqlvar[col].sqldata);
};

static inline strptr_t gds_get_strptr (gdsres_t *res, int col) {
    XSQLVAR *var = &res->out_sqlda->sqlvar[col];
    PARAMVARY *vary = (PARAMVARY*)var->sqldata;
    return (strptr_t){ .ptr = (char*)vary->vary_string, .len = vary->vary_length };
};

static inline const char *gds_get_str (gdsres_t *res, int col) {
    XSQLVAR *var = &res->out_sqlda->sqlvar[col];
    PARAMVARY *vary = (PARAMVARY*)var->sqldata;
    return (char*)vary->vary_string;
}

static inline float gds_get_f4 (gdsres_t *res, int col) {
    return *((float*)res->out_sqlda->sqlvar[col].sqldata);
}

static inline double gds_get_f8 (gdsres_t *res, int col) {
    return *((double*)res->out_sqlda->sqlvar[col].sqldata);
}

static inline int gds_get_date (gdsres_t *res, int col) {
    return *((ISC_DATE*)res->out_sqlda->sqlvar[col].sqldata);
};

static inline unsigned int gds_get_time (gdsres_t *res, int col) {
    return *((ISC_TIME*)res->out_sqlda->sqlvar[col].sqldata);
};

static inline ISC_TIMESTAMP gds_get_timestamp (gdsres_t *res, int col) {
    return *((ISC_TIMESTAMP*)res->out_sqlda->sqlvar[col].sqldata);
};

dbpool_t *gds_pool_create (const char *conn_info);
#define gds_pool_get(pool) (gdsconn_t*)dbpool_get(pool)
#define gds_pool_release(conn) dbpool_release((dbconn_t*)conn)

#endif
