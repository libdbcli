/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __LIBMDBCLI_H__
#define __LIBMDBCLI_H__

#include <stdint.h>
#include <stdarg.h>
#include <errno.h>
#include <libex/str.h>
#include <libex/list.h>
#include <mapi.h>
#include "dbcli.h"

typedef struct mdbconn mdbconn_t;
typedef struct mdbres mdbres_t;
struct mdbconn {
    dbpool_t *pool;
    time_t tm_released;
    list_item_t *li;
    int status;
    Mapi db;
};
mdbconn_t *mdb_connect (const char *conn_info);
void mdb_disconnect (mdbconn_t *conn);

typedef struct mdbfld mdbfld_t;
typedef void (*mdb_setfld_h) (mdbres_t*, int, mdbfld_t*);
struct mdbfld {
    char *ptr;
    int len;
    unsigned char type;
    unsigned int is_end;
};

#define MDB_TYPE 0
#define MDB_TYPE_STR 1
#define MDB_TYPE_DATE 2
#define MDB_TYPE_TIME 3
#define MDB_TYPE_TIMESTAMP 4

#define MDB(x,l) (mdbfld_t){ .len = l, .ptr = x , .type = MDB_TYPE, .is_end = 0 }
#define MDB_NULL (mdbfld_t){ .len = 0, .ptr = NULL, .type = MDB_TYPE, .is_end = 0 }
#define MDB_STR(x,l) (mdbfld_t){ .len = l, .ptr = x, .type = MDB_TYPE_STR, .is_end = 0 }
#define MDB_DATE(x,l) (mdbfld_t){ .len = l, .ptr = x, .type = MDB_TYPE_DATE, .is_end = 0 }
#define MDB_TIME(x,l) (mdbfld_t){ .len = l, .ptr = x, .type = MDB_TYPE_TIME, .is_end = 0 }
#define MDB_TIMESTAMP(x,l) (mdbfld_t){ .len = l, .ptr = x, .type = MDB_TYPE_TIMESTAMP, .is_end = 0 }
#define MDB_END (mdbfld_t){ .is_end = 1 }

struct mdbres {
    mdbconn_t *conn;
    MapiHdl stmt;
    int status;
    int qtype;
    char *sql;
    size_t sql_len;
    str_t *query;
    int nparam;
    long long int rows;
};
mdbres_t *mdb_prepare (mdbconn_t *conn, const char *sql, size_t sql_len);
int mdb_execp (mdbres_t *res, mdbfld_t arg, ...);
mdbres_t *mdb_exec (mdbconn_t *conn, const char *sql, size_t sql_len, mdbfld_t arg, ...);
long long int mdb_exec_sql (mdbconn_t *conn, const char *sql);
static inline int mdb_fetch (mdbres_t *res) {
    return mapi_fetch_row(res->stmt);
};
char *mdb_get (mdbres_t *res, int col);
void mdb_close (mdbres_t *res);

dbpool_t *mdb_pool_create (const char *conn_info);
#define mdb_pool_get(pool) (mdbconn_t*)dbpool_get(pool)
#define mdb_pool_release(conn) dbpool_release((dbconn_t*)conn)

#endif
