/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __LIBMYSQLCLI_H__
#define __LIBMYSQLCLI_H__

#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <mysql.h>
#include <libex/str.h>
#include <libex/list.h>
#include "dbcli.h"

typedef struct {
    dbpool_t *pool;
    time_t tm_released;
    list_item_t *li;
    int status;
    char *cmd;
    MYSQL db;
} myconn_t;
myconn_t *maria_connect (const char *conn_info);
void maria_disconnect (myconn_t *conn);

typedef struct {
    int status;
    MYSQL_STMT *stmt;
    MYSQL_RES *res;
    MYSQL_BIND *params;
    MYSQL_BIND *result;
    int nflds;
    int nparams;
    my_bool *param_nulls;
    my_bool *nulls;
    my_bool *errors;
    unsigned long *lengths;
    unsigned long *param_lengths;
} myres_t;
void maria_res_close (myres_t *res);

typedef union {
    int8_t      i1;
    uint8_t     ui1;
    int16_t     i2;
    uint16_t    ui2;
    int32_t     i4;
    uint32_t    ui4;
    int64_t     i8;
    uint64_t    ui8;
    float       f4;
    double      f8;
    strptr_t    s;
} myval_t;

typedef struct myfld myfld_t;
struct myfld {
    int is_null;
    int type;
    myval_t data;
} __attribute__ ((packed));

int maria_is_null (myres_t *res, int col);
char maria_get_tiny (myres_t *res, int col);
short int maria_get_smallint (myres_t *res, int col);
int maria_get_int (myres_t *res, int col);
long int maria_get_bigint (myres_t *res, int col);
float maria_get_float (myres_t *res, int col);
double maria_get_double (myres_t *res, int col);
strptr_t maria_get_str (myres_t *res, int col, int n);

#define MY_NULL (myfld_t){ .is_null = 1 }
#define MY_TINY(x) (myfld_t){ .is_null = 0, .type = MYSQL_TYPE_TINY, .data.i1 = x }
#define MY_SMALLINT(x) (myfld_t){ .is_null = 0, .type = MYSQL_TYPE_SHORT, .data.i2 = x }
#define MY_INT(x) (myfld_t) { .is_null = 0, .type = MYSQL_TYPE_LONG, .data.i4 = x }
#define MY_BIGINT(x) (myfld_t){ .is_null = 0, .type = MYSQL_TYPE_LONGLONG, .data.i8 = x }
#define MY_FLOAT(x) (myfld_t){ .is_null = 0, .type = MYSQL_TYPE_FLOAT, .data.f4 = x }
#define MY_DOUBLE(x) (myfld_t){ .is_null = 0, .type = MYSQL_TYPE_DOUBLE, .data.f8 = x }
#define MY_STR(x,l) (myfld_t){ .is_null = 0, .type = MYSQL_TYPE_VAR_STRING, .data.s.ptr = x, .data.s.len = l }
#define MY_END (myfld_t){ .is_null = 0, .type = 0 }

myres_t *maria_prepare (myconn_t *conn, const char *sql, size_t sql_len);
myres_t *maria_exec (myconn_t *conn, const char *sql, size_t sql_len, myfld_t arg, ...);
int maria_pexec (myres_t *res, myfld_t arg, ...);
int maria_exec_sql (myconn_t *conn, const char *sql, size_t sql_len);
int maria_fetch (myres_t *res);
static inline int maria_start (myconn_t *conn) { return maria_exec_sql(conn, CONST_STR_LEN("start transaction")); }
static inline int maria_commit (myconn_t *conn) { return maria_exec_sql(conn, CONST_STR_LEN("commit")); }
static inline int maria_rollback (myconn_t *conn) { return maria_exec_sql(conn, CONST_STR_LEN("rollback")); }

dbpool_t *maria_pool_create (const char *conn_info);
#define my_pool_get(pool) (myconn_t*)dbpool_get(pool)
#define my_pool_release(conn) dbpool_release((dbconn_t*)conn)

#endif
