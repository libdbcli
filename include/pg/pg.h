/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __LIBPGCLI_H__
#define __LIBPGCLI_H__

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <time.h>
#include <endian.h>
#include <pthread.h>
#include <gmp.h>
#include <libpq-fe.h>
#include <pgtypes_date.h>
#include <pgtypes_numeric.h>
#include <pgtypes_timestamp.h>
#include <uuid/uuid.h>
#include <libex/str.h>
#include <libex/list.h>
#include "pgconsts.h"
#include "pgerrcodes.h"
#include "dbcli.h"

typedef struct pgpool pgpool_t;
typedef struct pgres pgres_t;
typedef struct {
    dbpool_t *pool;
    time_t tm_released;
    list_item_t *li;
    int status;
    char *cmd;
    PGconn *db;
} pgconn_t;
pgconn_t *pg_connect (const char *conn_info);
void pg_disconnect (pgconn_t *conn);

typedef struct {
    int64_t time;
    int32_t day;
    int32_t month;
} interval_t;

typedef struct {
    int32_t len;
    uint32_t bit;
} bit32_t;

typedef struct {
    mpq_t num;
    short dscale;
} numeric_t;
static inline void pg_clear_numeric (numeric_t *x) {
    mpq_clear(x->num);
}

#define NUMERIC_POS 0x0000
#define NUMERIC_NEG 0x4000
#define NUMERIC_SHORT 0x8000
#define NUMERIC_DSCALE_MASK 0x3FFF

#define PG_NOCOPY 0
#define PG_COPY 1

#define PG_DEFAULT 0

#define PG_READ_COMMITTED 1
#define PG_REPEATABLE_READ 2
#define PG_SERIALIZABLE 3
#define PG_READ_UNCOMMITTED 4

#define PG_READ_WRITE 1
#define PG_READ_ONLY 2

typedef union {
    int32_t        i4;
    int64_t        i8;
    float          f4;
    double         f8;
    char          *s;
    int8_t         b;
    int32_t        d;
    timestamp      tm;
    interval_t     intv;
    mpq_t          q;
    bit32_t        bit32;
    int            addr;
    unsigned char *uuid;
} pgval_t;

typedef struct {
    int params_len;
    Oid *types;
    int *lens;
    int *fmts;
    const char **vals;
    unsigned char *is_free;
} pg_params_t;

typedef struct pgfld pgfld_t;
typedef void (*pg_setfld_h) (pgfld_t*, int, pg_params_t*);
struct pgfld {
    unsigned char is_null;
    int type;
    pgval_t data;
    int len;
    pg_setfld_h setfld;
} __attribute__ ((packed));

int pg_isnull (pgres_t *res, int row, int col);
int pg_geti4 (pgres_t *res, int row, int col);
long int pg_geti8 (pgres_t *res, int row, int col);
float pg_getf4 (pgres_t *res, int row, int col);
double pg_getf8 (pgres_t *res, int row, int col);
strptr_t pg_getstr (pgres_t *res, int row, int col, int copy_data);
const char *pg_getstrptr (pgres_t *res, int reow, int col);
int pg_getbool (pgres_t *res, int row, int col);
int pg_getdate (pgres_t *res, int row, int col);
unsigned int pg_getbit (pgres_t *res, int row, int col);
timestamp pg_gettm (pgres_t *res, int row, int col);
interval_t pg_getintv (pgres_t *res, int row, int col);
unsigned int pg_getinet (pgres_t *res, int row, int col);
numeric_t pg_getnum (pgres_t *res, int row, int col);
unsigned char *pg_getuuid (pgres_t *res, int row, int col);

int pg_add_param (clist_t *params, pgfld_t arg);
int pg_add_param_type (clist_t *params, Oid arg);

struct pgres {
    pgconn_t *conn;
    int status;
    PGresult *stmt;
    const char *name;
    PGresult *stmt_prepared;
    int nrecs;
    int nflds;
    int nrows;
    int nparams;
};
void pg_close (pgres_t *res);
pgres_t *pg_exec (pgconn_t *conn, const char *sql, pgfld_t arg, ...);
pgres_t *pg_execl (pgconn_t *conn, const char *sql, clist_t *args);
pgres_t *pg_prepare (pgconn_t *conn, const char *name, const char *sql, int arg, ...);
pgres_t *pg_preparel (pgconn_t *conn, const char *name, const char *sql, clist_t *args);
int pg_execp (pgres_t *res, pgfld_t arg, ...);
int pg_execpl (pgres_t *res, clist_t *args);
int pg_exec_sql (pgconn_t *conn, const char *sql);

int pg_start (pgconn_t *conn, int level, int mode);
static inline int pg_commit (pgconn_t *conn) {
    return pg_exec_sql(conn, "commit");
};
static inline int pg_rollback (pgconn_t *conn) {
    return pg_exec_sql(conn, "rollback");
}

void pg_set (pgfld_t *fld, int col, pg_params_t *params);
void pg_seti4 (pgfld_t *fld, int col, pg_params_t *params);
void pg_seti8 (pgfld_t *fld, int col, pg_params_t *params);
void pg_setf4 (pgfld_t *fld, int col, pg_params_t *params);
void pg_setf8 (pgfld_t *fld, int col, pg_params_t *params);
void pg_setstr (pgfld_t *fld, int col, pg_params_t *params);
void pg_setbytea (pgfld_t *fld, int col, pg_params_t *params);
void pg_setbool (pgfld_t *fld, int col, pg_params_t *params);
void pg_setdate (pgfld_t *fld, int col, pg_params_t *params);
void pg_settimestamp (pgfld_t *fld, int col, pg_params_t *params);
void pg_setuuid (pgfld_t *fld, int col, pg_params_t *params);
void pg_setbytea (pgfld_t *fld, int col, pg_params_t *params);
void pg_setbit (pgfld_t *fld, int col, pg_params_t *params);

#define PG(x) (pgfld_t){ .is_null=1, .type = 0, .setfld = pg_set, .data.s = x }
#define PG_NULL (pgfld_t){ .is_null = 1, .type = 0, .setfld = pg_set }
#define PG_I4(x) (pgfld_t){ .is_null = 0, .type = OID_INT4, .setfld = pg_seti4, .data.i4 = x }
#define PG_I8(x) (pgfld_t){ .is_null = 0, .type = OID_INT8, .setfld = pg_seti8, .data.i8 = x }
#define PG_F4(x) (pgfld_t){ .is_null = 0, .type = OID_FLOAT4, .setfld = pg_setf4, .data.f4 = x }
#define PG_F8(x) (pgfld_t){ .is_null = 0, .type = OID_FLOAT8, .setfld = pg_setf8, .data.f8 = x }
#define PG_STR(x,l) (pgfld_t){ .is_null = 0, .type = OID_VARCHAR, .setfld = pg_setstr, .data.s = x, .len = l }
#define PG_CHR(x,l) (pgfld_t){ .is_null = 0, .type = OID_CHAR, .setfld = pg_setstr, .data.s = x, .len = l }
#define PG_TXT(x,l) (pgfld_t){ .is_null = 0, .type = OID_TEXT, .setfld = pg_setstr, .data.s = x, .len = l }
#define PG_BYTEA(x,l) (pgfld_t){ .is_null = 0, .type = OID_BYTEA, .setfld = pg_setbytea, .data.s = x, .len = l }
#define PG_BOOL(x) (pgfld_t){ .is_null = 0, .type = OID_BOOL, .setfld = pg_setbool, .data.b = x }
#define PG_DATE(x) (pgfld_t){ .is_null = 0, .type = OID_DATE, .setfld = pg_setdate, .data.d = x }
#define PG_TM(x) (pgfld_t){ .is_null = 0, .type = OID_TIMESTAMP, .setfld = pg+settimestamp, .data.tm = x }
#define PG_UUID(x) (pgfld_t){ .is_null = 0, .type = OID_UUID, .setfld = pg_setuuid, .data.uuid = x }
// TODO : now only 32 bit
#define PG_BIT(x) (pgfld_t){ .is_null = 0, .type = OID_BIT, .setfld = pg_setbit, .data.bit32.bit = x, .data.bit32.len = sizeof(uint32_t) }
#define PG_MONEY(x) (pgfld_t){ .is_null = 0, .type = OID_MONEY, .setfld = pg_seti8, .data.i8 = x }
#define PG_END (pgfld_t){ .is_null = 0, .setfld = NULL }

dbpool_t *pg_pool_create (const char *conn_info);
#define pg_pool_get(pool) (pgconn_t*)dbpool_get(pool)
#define pg_pool_release(conn) dbpool_release((dbconn_t*)conn)

#endif
