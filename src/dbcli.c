/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "dbcli.h"

__thread long dbcli_code = 0;
__thread char dbcli_state [DBCLI_STATE_LEN] = {0};
__thread char dbcli_msg [DBCLI_MSG_LEN] = {0};

int parse_conn_info (const char *conn_info, conn_info_t *info) {
    char *str = (char*)conn_info;
    size_t str_len = strlen(str);
    strptr_t tok;
    memset(info, 0, sizeof(conn_info_t));
    while (0 == strntok(&str, &str_len, CONST_STR_LEN("= "), &tok)) {
        char **p = NULL;
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("HOST"))) {
            if (-1 == strntok(&str, &str_len, CONST_STR_LEN("= "), &tok))
                goto err;
            p = &info->s_host;
        } else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("PORT"))) {
            if (-1 == strntok(&str, &str_len, CONST_STR_LEN("= "), &tok))
                goto err;
            p = &info->s_port;
        } else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("USER"))) {
            if (-1 == strntok(&str, &str_len, CONST_STR_LEN("= "), &tok))
                goto err;
            p = &info->s_user;
        } else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("PASS"))) {
            if (-1 == strntok(&str, &str_len, CONST_STR_LEN("= "), &tok))
                goto err;
            p = &info->s_pass;
        } else
        if (0 == cmpcasestr(tok.ptr, tok.len, CONST_STR_LEN("DBNAME"))) {
            if (-1 == strntok(&str, &str_len, CONST_STR_LEN("= "), &tok))
                goto err;
            p = &info->s_dbname;
        }
        if (p)
            *p = strndup(tok.ptr, tok.len);
    }
    if (info->s_port && -1 == (info->port = atoport(info->s_port, "tcp")))
        goto err;
    return 0;
err:
    clear_conn_info(info);
    return -1;
}

void clear_conn_info (conn_info_t *info) {
    if (info->s_host) free(info->s_host);
    if (info->s_port) free(info->s_port);
    if (info->s_user) free(info->s_user);
    if (info->s_pass) free(info->s_pass);
    if (info->s_dbname) free(info->s_dbname);
    memset(info, 0, sizeof(conn_info_t));
}

//******************************************************************************
// Connection pool
//******************************************************************************
dbpool_t *dbpool_create () {
    dbpool_t *pool = calloc(1, sizeof(dbpool_t));
    if (!pool) return NULL;
    pool->pg_free = lst_alloc(NULL);
    pool->pg_busy = lst_alloc(NULL);
    pthread_mutex_init(&pool->locker, NULL);
    pthread_condattr_init(&pool->cond_attr);
    pthread_condattr_setclock(&pool->cond_attr, CLOCK_MONOTONIC);
    pthread_cond_init(&pool->cond, &pool->cond_attr);
    return pool;
}

void dbpool_setopt_hnd (dbpool_t *pool, dbpool_opt_t opt, dbpool_h arg) {
    switch (opt) {
        case DBPOOL_ERROR:
            pool->on_error = arg;
            break;
        case DBPOOL_DISCONNECT:
            pool->on_disconnect = arg;
            break;
        default:
            break;
    }
}

void dbpool_setopt_str (dbpool_t *pool, dbpool_opt_t opt, const char *arg) {
    if (pool->conn_info)
        free(pool->conn_info);
    pool->conn_info = strdup(arg);
}

void dbpool_setopt_int (dbpool_t *pool, dbpool_opt_t opt, long long int arg) {
    switch (opt) {
        case DBPOOL_LIVINGTIME:
            pool->livingtime = arg;
            break;
        case DBPOOL_MAXLEN:
            pool->maxlen = arg;
            break;
        default:
            break;
    }
}

void dbpool_setopt_con (dbpool_t *pool, dbpool_opt_t opt, dbpool_connect_h arg) {
    pool->on_connect = arg;
}

static void *on_dbpool (void *param) {
    dbpool_t *pool = (dbpool_t*)param;
    while (pool->is_alive) {
        struct timespec to_time;
        clock_gettime(CLOCK_MONOTONIC, &to_time);
        to_time.tv_sec += pool->livingtime;
        if (ETIMEDOUT == pthread_cond_timedwait(&pool->cond, &pool->locker, &to_time)) {
            list_t *lst;
            list_item_t *li;
            time_t t0 = time(0);
            pthread_mutex_lock(&pool->locker);
            lst = pool->pg_free;
            if ((li = lst->head)) {
                do {
                    list_item_t *li_n = li->next;
                    dbconn_t *conn = (dbconn_t*)li->ptr;
                    if (conn->tm_released + pool->livingtime < t0) {
                        lst_del(li);
                        pool->on_disconnect(conn);
                    }
                    li = li_n;
                } while (li != lst->head);
            }
            pthread_mutex_unlock(&pool->locker);
        }
    }
    return NULL;
}

int dbpool_start (dbpool_t *pool) {
    if (!pool->on_connect || !pool->on_disconnect)
        return -1;
    if (0 == pool->livingtime)
        return 0;
    pool->is_alive = 1;
    return pthread_create(&pool->th, NULL, on_dbpool, (void*)pool);
}

static int on_dbpool_disconnect (list_item_t *li, void *userdata) {
    dbpool_t *pool = userdata;
    if (pool->on_disconnect)
        pool->on_disconnect(li->ptr);
    return ENUM_CONTINUE;
}

void dbpool_free (dbpool_t *pool) {
    pthread_t th = 0;
    if (0 != memcmp(&th, &pool->th, sizeof(pthread_t))) {
        pthread_mutex_lock(&pool->locker);
        pool->is_alive = 0;
        pthread_cond_signal(&pool->cond);
        pthread_mutex_unlock(&pool->locker);
        pthread_join(pool->th, NULL);
    }
    lst_enum(pool->pg_free, on_dbpool_disconnect, pool, 0);
    lst_enum(pool->pg_busy, on_dbpool_disconnect, pool, 0);
    lst_free(pool->pg_free);
    lst_free(pool->pg_busy);
    if (pool->conn_info)
        free(pool->conn_info);
    pthread_mutex_destroy(&pool->locker);
    pthread_condattr_destroy(&pool->cond_attr);
    pthread_cond_destroy(&pool->cond);
    free(pool);
}

static dbconn_t *dbpool_connect (dbpool_t *pool) {
    dbconn_t *conn = pool->on_connect(pool->conn_info);
    if (-1 == conn->status) {
        if (pool->on_error)
            pool->on_error(conn);
        pool->on_disconnect(conn);
        pthread_mutex_unlock(&pool->locker);
        return NULL;
    }
    conn->li = lst_add(pool->pg_busy, conn);
    conn->pool = pool;
    pthread_mutex_unlock(&pool->locker);
    return conn;
}

dbconn_t *dbpool_get (dbpool_t *pool) {
    dbconn_t *conn;
    list_item_t *li;
    pthread_mutex_lock(&pool->locker);
    if (!(li = pool->pg_free->head))
        return dbpool_connect(pool);
    conn = (dbconn_t*)li->ptr;
    lst_del(li);
    conn->tm_released = 0;
    conn->li = lst_add(pool->pg_busy, conn);
    pthread_mutex_unlock(&pool->locker);
    return conn;
}

void dbpool_release (dbconn_t *conn) {
    pthread_mutex_lock(&conn->pool->locker);
    lst_del(conn->li);
    conn->tm_released = time(0);
    conn->li = lst_adde(conn->pool->pg_free, conn);
    pthread_mutex_unlock(&conn->pool->locker);
}
