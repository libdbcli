/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gds/gds.h"

/*static void gds_error (ISC_STATUS_ARRAY status, long *code, char **msg) {
    char buf [512];
    *code = isc_sqlcode(status);
    isc_sql_interprete(*code, buf, sizeof buf);
    if (*msg) free(*msg);
    *msg = strdup(buf);
}*/
void gds_error (ISC_STATUS_ARRAY status) {
    dbcli_code = isc_sqlcode(status);
    isc_sql_interprete(dbcli_code, dbcli_msg, DBCLI_MSG_LEN);
}

//******************************************************************************
// Database connection
//******************************************************************************
gdsconn_t *gds_connect (const char *conn_info) {
    char *dpb, *p_dpb = NULL, buf [256];
    short dpb_len = 0, dpb_size;
    conn_info_t info;
    gdsconn_t *conn = calloc(1, sizeof(gdsconn_t));
    if (-1 == parse_conn_info(conn_info, &info)) {
        clear_conn_info(&info);
        dbcli_code = conn->status = -1;
        strcpy(dbcli_msg, "Connection parameter(s) error");
        return conn;
    }
    
    if (!info.s_host)
        info.s_host = strndup(CONST_STR_LEN("localhost"));
    if (!info.s_user)
        info.s_user = strndup(CONST_STR_LEN("sysdba"));
    if (!info.s_pass)
        goto done;
    if (!info.s_dbname) {
        info.s_dbname = malloc(64);
        snprintf(info.s_dbname, 64, "%s.gdb", getenv("USER"));
    }
    snprintf(buf, sizeof buf, "%s:%s", info.s_host, info.s_dbname);

    ISC_STATUS_ARRAY status;
    ISC_STATUS rc;
    int user_len = strlen(info.s_user),
        pass_len = strlen(info.s_pass);
    dpb_size = user_len + pass_len + 24;
    p_dpb = dpb = malloc(dpb_size);
    dpb_len = 1;
    *dpb = isc_dpb_version1;
    isc_modify_dpb(&dpb, &dpb_len, isc_dpb_user_name, info.s_user, user_len);
    isc_modify_dpb(&dpb, &dpb_len, isc_dpb_password, info.s_pass, pass_len);
    if (0 != (rc = isc_attach_database(status, strlen(buf), buf, &conn->db, dpb_len, dpb))) {
        conn->status = -1;
        gds_error(status);
    }

done:
    if (p_dpb) free(p_dpb);
    clear_conn_info(&info);
    return conn;
}

void gds_disconnect (gdsconn_t *conn) {
    ISC_STATUS_ARRAY status;
    if (conn->db)
        isc_detach_database(status, (isc_db_handle*)&conn->db);
    free(conn);
}

//******************************************************************************
// Transaction
//******************************************************************************
char gds_concurrency [] = {
    isc_tpb_version3, isc_tpb_write, isc_tpb_concurrency
};
char gds_read_committed [] = {
    isc_tpb_version3, isc_tpb_write, isc_tpb_read_committed, isc_tpb_rec_version
};

gdstr_t gds_start (char *tpb, int tpb_len, gdsconn_t *conn, ...) {
    gdstr_t tr;
    ISC_STATUS rc;
    ISC_STATUS_ARRAY status;
    int db_handle_count = 1, i = 0;
    isc_db_handle *dbs;
    va_list ap;
    va_start(ap, conn);
    while (1) {
        gdsconn_t *x = va_arg(ap, gdsconn_t*);
        if (!x)
            break;
        ++db_handle_count;
    }
    va_end(ap);
    dbs = malloc(db_handle_count * sizeof(isc_db_handle));
    dbs[i++] = conn->db;
    va_start(ap, conn);
    while (1) {
        gdsconn_t *x = va_arg(ap, gdsconn_t*);
        if (!x)
            break;
        dbs[i++] = x->db;
    }
    va_end(ap);
    if (0 != (rc = isc_start_transaction(status, &tr, db_handle_count, dbs, tpb_len, tpb))) {
        conn->status = -1;
        gds_error(status);
    }
    free(dbs);
    return tr;
}

//******************************************************************************
// Statement
//******************************************************************************
gdsres_t *gds_prepare (gdsconn_t *conn, gdstr_t *tr, const char *sql, size_t sql_len) {
    ISC_STATUS rc;
    ISC_STATUS_ARRAY status;
    int nparams = 1;
    gdsres_t *res = calloc(1, sizeof(gdsres_t));
    XSQLDA *in_sqlda = res->in_sqlda = malloc(XSQLDA_LENGTH(nparams)),
           *out_sqlda = res->out_sqlda = malloc(XSQLDA_LENGTH(20));
    res->conn = conn;
    memset(in_sqlda, 0, XSQLDA_LENGTH(1));
    in_sqlda->sqln = 1;
    in_sqlda->version = out_sqlda->version = SQLDA_VERSION1;
    out_sqlda->sqln = 20;
    if (!tr) {
        if (0 != (rc = isc_start_transaction(status, &res->tr, 1, &conn->db, 0, 1))) {
            gds_error(status);
            res->status = -1;
            return res;
        }
        res->is_auto = 1;
        tr = &res->tr;
    }
    if (0 != (rc = isc_dsql_allocate_statement(status, &conn->db, &res->stmt)) ||
        0 != (rc = isc_dsql_prepare(status, tr, &res->stmt, sql_len, sql, SQL_DIALECT_V6, out_sqlda)) ||
        0 != (rc = isc_dsql_describe_bind(status, &res->stmt, SQL_DIALECT_V6, in_sqlda))) {
        gds_error(status);
        res->status = -1;
        return res;
    }
    if (in_sqlda->sqld > in_sqlda->sqln) {
        nparams = in_sqlda->sqld;
        if (!(in_sqlda = realloc(in_sqlda, XSQLDA_LENGTH(nparams)))) {
            if (res->is_auto)
                isc_rollback_transaction(status, &res->tr);
            dbcli_code = res->status = -1;
            strcpy(dbcli_msg, "Out of memory");
            return res;
        }
        #ifndef NDEBUG
        memset(&in_sqlda->sqlvar[0], 0, sizeof(XSQLVAR)*nparams);
        #endif
        res->in_sqlda = in_sqlda;
    }
    if (out_sqlda->sqld > out_sqlda->sqln) {
        if (!(out_sqlda = realloc(out_sqlda, XSQLDA_LENGTH(out_sqlda->sqld)))) {
            if (res->is_auto)
                isc_rollback_transaction(status, &res->tr);
            dbcli_code = res->status = -1;
            strcpy(dbcli_msg, "Out of memory");
            return res;
        }
        res->out_sqlda = out_sqlda;
    }
    if (out_sqlda->sqld > 0) {
        res->out_ind = malloc(out_sqlda->sqld * sizeof(short));
        for (int i = 0; i < out_sqlda->sqld; ++i) {
            out_sqlda->sqlvar[i].sqlind = &res->out_ind[i];
            out_sqlda->sqlvar[i].sqldata = malloc(out_sqlda->sqlvar[i].sqllen + 2);
        }
    }
    return res;
}

static int set_params (gdsres_t *res, gdsfld_t *arg, va_list ap) {
    int col = 0;
    if (!arg->setfld) {
        if (col == res->in_sqlda->sqld)
            return 0;
        return -1;
    }
    arg->setfld(res, col++, arg);
    while (1) {
        gdsfld_t fld = va_arg(ap, gdsfld_t);
        if (fld.setfld) {
            if (col < res->in_sqlda->sqld)
                fld.setfld(res, col++, &fld);
            else
                return -1;
        } else {
            if (col == res->in_sqlda->sqld)
                return 0;
            return-1;
        }
    }
    return -1;
}

int gds_exec (gdsres_t *res, gdstr_t *tr, gdsfld_t arg, ...) {
    int rc;
    ISC_STATUS ret;
    ISC_STATUS_ARRAY status;
    XSQLDA *in_sqlda = res->in_sqlda->sqld > 0 ? res->in_sqlda : NULL,
           *out_sqlda = res->out_sqlda->sqld > 0 ? res->out_sqlda : NULL;
    if (in_sqlda) {
        va_list ap;
        va_start(ap, arg);
        res->in_vars = calloc(in_sqlda->sqld, sizeof(char*));
        rc = set_params(res, &arg, ap);
        va_end(ap);
        if (-1 == rc) {
            dbcli_code = res->status = -1;
            strcpy(dbcli_msg, "Parameter count error");
            return -1;
        }
    }
    rc = -1;
    if (res->is_auto)
        tr = &res->tr;
    ret = isc_dsql_execute(status, tr, &res->stmt, SQL_DIALECT_V6, in_sqlda);
    if (0 == ret)
        rc = out_sqlda ? 1 : 0;
    else {
        gds_error(status);
        res->status = -1;
    }
    if (res->in_vars) {
        for (int i = 0; i < in_sqlda->sqld; ++i)
            if (res->in_vars[i])
                free(res->in_vars[i]);
        free(res->in_vars);
        res->in_vars = NULL;
    }
    if (res->is_auto && rc < 0) {
        isc_rollback_transaction(status, &res->tr);
        res->tr = 0;
    }
    return rc;
}

int gds_fetch (gdsres_t *res) {
    ISC_STATUS_ARRAY status;
    ISC_STATUS rc = isc_dsql_fetch(status, &res->stmt, SQL_DIALECT_V6, res->out_sqlda);
    if (100 == rc)
        return 0;
    if (0 == rc)
        return 1;
    gds_error(status);
    res->status = -1;
    return -1;
}

void gds_close (gdsres_t *res) {
    ISC_STATUS_ARRAY status;
    if (res->is_auto && res->tr)
        isc_commit_transaction(status, &res->tr);
    if (res->stmt)
        isc_dsql_free_statement(status, &res->stmt, DSQL_close);
    if (res->in_sqlda)
        free(res->in_sqlda);
    if (res->out_sqlda) {
        for (int i = 0; i < res->out_sqlda->sqld; ++i)
            free(res->out_sqlda->sqlvar[i].sqldata);
        free(res->out_sqlda);
    }
    if (res->out_ind)
        free(res->out_ind);
    free(res);
}

int gds_exec_sql (gdsconn_t *conn, isc_tr_handle *tr, const char *sql, size_t sql_len) {
    int rc = 0;
    ISC_STATUS res;
    ISC_STATUS_ARRAY status;
    isc_tr_handle tr_handle = 0;
    if (!tr) {
        tr = &tr_handle;
        if (0 != (res = isc_start_transaction(status, tr, 1, &conn->db, 0, 1))) {
            gds_error(status);
            return -1;
        }
    }
    if (0 != (res = isc_dsql_execute_immediate(status, &conn->db, tr, sql_len, sql, SQL_DIALECT_V6, NULL))) {
        gds_error(status);
        if (tr == &tr_handle)
            isc_rollback_transaction(status, tr);
        rc = -1;
    } else
    if (tr == &tr_handle)
        isc_commit_transaction(status, tr);
    return rc;
}

//******************************************************************************
// Set data
//******************************************************************************
void gds_set_bool (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.b;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_UCHAR);
    var->sqltype = SQL_BOOLEAN;
}

void gds_set_i2 (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.i2;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_SHORT);
    var->sqltype = SQL_SHORT;
}

void gds_set_i4 (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.i4;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_LONG);
    var->sqltype = SQL_LONG;
}

void gds_set_i8 (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.i8;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_INT64);
    var->sqltype = SQL_INT64;
}

void gds_set_str (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    if (fld->len > 0) {
        res->in_vars[col] = strndup(fld->val.s, fld->len);
        fld->val.s = res->in_vars[col];
    } else
        fld->len = strlen(fld->val.s);
    var->sqldata = fld->val.s;
    var->sqlind = &fld->ind;
    var->sqllen = fld->len;
    var->sqltype = SQL_TEXT;
}

void gds_set_f4 (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.f4;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(float);
    var->sqltype = SQL_FLOAT;
}

void gds_set_f8 (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.i8;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(double);
    var->sqltype = SQL_DOUBLE;
}

void gds_set_dt (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.dt;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_DATE);
    var->sqltype = SQL_TYPE_DATE;
}

void gds_set_tm (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.tm;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_TIME);
    var->sqltype = SQL_TYPE_TIME;
}

void gds_set_ts (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqldata = (ISC_SCHAR*)&fld->val.ts;
    var->sqlind = &fld->ind;
    var->sqllen = sizeof(ISC_TIMESTAMP);
    var->sqltype = SQL_TIMESTAMP;
}

void gds_set_null (gdsres_t *res, int col, gdsfld_t *fld) {
    XSQLVAR *var = &res->in_sqlda->sqlvar[col];
    var->sqlind = &fld->ind;
    var->sqltype = SQL_NULL;
}

//******************************************************************************
// Get data
//******************************************************************************
short gds_type (gdsres_t *res, int col) {
    if (res->out_sqlda && col >= 0 && col < res->out_sqlda->sqld) {
        XSQLVAR *var = &res->out_sqlda->sqlvar[col];
        return var->sqltype & ~1;
    }
    return -1;
}

#if __x86_64 || __ppc64__
    #define ISC_INT64_FORMAT "l"
#else
    #define ISC_INT64_FORMAT "ll"
#endif
#define SHORT_SIZE 6
#define LONG_SIZE 11
#define INT64_SIZE 21
#define FLOAT_SIZE 15
#define DOUBLE_SIZE 24
#define TIMESTAMP_SIZE 24
#define DATE_SIZE 13
#define TIME_SIZE 14
#define BLOB_SIZE 17
static char *get_varying (XSQLVAR *var, size_t *len) {
    PARAMVARY *vary = (PARAMVARY*)var->sqldata;
    char *str = strndup((char*)vary->vary_string, vary->vary_length);
    if (len)
        *len = vary->vary_length;
    return str;
}

static char *get_num (XSQLVAR *var, ISC_INT64 val, short width, size_t *len) {
    short dscale = var->sqlscale;
    char *str = malloc(width + 1);
    int n = 0;
    if (dscale < 0) {
        ISC_INT64 tens = 1;
        for (int i = 0; i > dscale; ++i)
            tens *= 10;
        if (val >= 0)
            n = snprintf(str, width + 1, "%lld.%0*lld", val / tens, -dscale, val % tens);
        else
        if (0 != val / tens)
            n = snprintf(str, width + 1, "%lld.%0*lld", val / tens, -dscale, -(val % tens));
        else
            n = snprintf(str, width + 1, "%s.%0*lld", "-0", -dscale, -(val % tens));
    } else
    if (dscale)
        n = snprintf(str, width + 1, "%lld%0*d", val, dscale, 0);
    else
        n = snprintf(str, width + 1, "%lld", val);
    if (len)
        *len = n;
    return str;
}

static char *get_timestamp (XSQLVAR *var, size_t *len) {
    struct tm tm;
    char *str = malloc(TIMESTAMP_SIZE + 1);
    int n;
    isc_decode_timestamp((ISC_TIMESTAMP*)var->sqldata, &tm);
    n = snprintf(str, TIMESTAMP_SIZE + 1, "%04d-%02d-%02d %02d:%02d:%02d.%04d", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday,
        tm.tm_hour, tm.tm_min, tm.tm_sec, ((ISC_TIMESTAMP*)var->sqldata)->timestamp_time * 10000);
    if (len)
        *len = n;
    return str;
}

static char *get_date (XSQLVAR *var, size_t *len) {
    struct tm tm;
    char *str = malloc(DATE_SIZE + 1);
    int n;
    isc_decode_sql_date((ISC_DATE*)var->sqldata, &tm);
    n = snprintf(str, DATE_SIZE + 1, "%04d-%02d-%02d", tm.tm_year+1900, tm.tm_mon, tm.tm_mday);
    if (len)
        *len = n;
    return str;
}

static char *get_time (XSQLVAR *var, size_t *len) {
    struct tm tm;
    char *str = malloc(TIME_SIZE + 1);
    int n;
    isc_decode_sql_time((ISC_TIME*)var->sqldata, &tm);
    n = snprintf(str, TIME_SIZE + 1, "%02d:%02d:%02d.%04d", tm.tm_hour, tm.tm_min, tm.tm_sec, (*((ISC_TIME*)var->sqldata)) % 10000);
    if (len)
        *len = n;
    return str;
}

static char *get_blob (XSQLVAR *var, size_t *len) {
    ISC_QUAD *bid = (ISC_QUAD*)var->sqldata;
    char *str = malloc(BLOB_SIZE + 1);
    int n = snprintf(str, BLOB_SIZE + 1, "%08x:%08x", bid->gds_quad_high, bid->gds_quad_low);
    if (len)
        *len = n;
    return str;
}

char *gds_get (gdsres_t *res, int col, size_t *len) {
    char *str = NULL;
    int n;
    XSQLVAR *var = &res->out_sqlda->sqlvar[col];
    if ((var->sqltype & 1) && *var->sqlind < 0)
        return NULL;
    switch (var->sqltype & ~1) {
        case SQL_TEXT:
            str = strndup(var->sqldata, var->sqllen);
            if (len)
                *len = var->sqllen;
            break;
        case SQL_VARYING:
            str = get_varying(var, len);
            break;
        case SQL_SHORT:
            str = get_num(var, *(short*)var->sqldata, SHORT_SIZE, len);
            break;
        case SQL_LONG:
            str = get_num(var, *(int*)var->sqldata, LONG_SIZE, len);
            break;
        case SQL_INT64:
            str = get_num(var, *(ISC_INT64*)var->sqldata, INT64_SIZE, len);
            break;
        case SQL_FLOAT:
            str = malloc(FLOAT_SIZE + 1);
            n = snprintf(str, FLOAT_SIZE + 1, "%g", *(float*)var->sqldata);
            if (len)
                *len = n;
            break;
        case SQL_DOUBLE:
            str = malloc(DOUBLE_SIZE + 1);
            n = snprintf(str, DOUBLE_SIZE + 1, "%f", *(double*)var->sqldata);
            if (len)
                *len = n;
            break;
        case SQL_TIMESTAMP:
            str = get_timestamp(var, len);
            break;
        case SQL_TYPE_DATE:
            str = get_date(var, len);
            break;
        case SQL_TYPE_TIME:
            str = get_time(var, len);
            break;
        case SQL_BLOB:
        case SQL_ARRAY:
            str = get_blob(var, len);
            break;
        default:
            break;
    }
    return str;
}

//******************************************************************************
// Connection pool
//******************************************************************************
dbpool_t *gds_pool_create (const char *conn_info) {
    dbpool_t *pool = dbpool_create();
    dbpool_setopt(pool, DBPOOL_CONNECT, (dbpool_connect_h)gds_connect);
    dbpool_setopt(pool, DBPOOL_DISCONNECT, (dbpool_h)gds_disconnect);
    dbpool_setopt(pool, DBPOOL_CONNECT, conn_info);
    return pool;
}
