/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mdb/mdb.h"

//******************************************************************************
// Database connection
//******************************************************************************
mdbconn_t *mdb_connect (const char *conn_info) {
    mdbconn_t *conn = calloc(1, sizeof(mdbconn_t));
    conn_info_t info;
    if (-1 == parse_conn_info(conn_info, &info)) {
        clear_conn_info(&info);
        dbcli_code = conn->status = -1;
        strcpy(dbcli_msg, "Connection parameter(s) error");
        return conn;
    }
    if (!info.s_host)
        info.s_host = strndup(CONST_STR_LEN("localhost"));
    if (!info.port)
        info.port = 50000;
    if (!(conn->db = mapi_connect(info.s_host, info.port, info.s_user, info.s_pass, "sql", info.s_dbname))) {
        dbcli_code = conn->status = -1;
        strcpy(dbcli_msg, "Wount connect to server");
    } else
    if (mapi_error(conn->db)) {
        dbcli_code = conn->status = -1;
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", (char*)mapi_error_str(conn->db));
    }
    clear_conn_info(&info);
    return conn;
}

void mdb_disconnect (mdbconn_t *conn) {
    if (conn->db) {
        if (MOK == mapi_disconnect(conn->db))
            mapi_destroy(conn->db);
    }
    free(conn);
}

//******************************************************************************
// Statement
//******************************************************************************
static int get_param_count (const char *p, size_t len) {
    const char *e = p + len;
    int rc = 0;
    while (p < e) {
        if (*p == '\'')
            while (p < e && '\'' != *p) ++p;
        if (p == e)
            break;
        if ('?' == *p)
            ++rc;
        ++p;
    }
    return rc;
}

static char *find_next_pholder (strptr_t *s) {
    char *e = s->ptr + s->len;
    while (s->ptr < e) {
        if (*s->ptr == '\'')
            while (s->ptr < e && '\'' != *s->ptr) { ++s->ptr; --s->len; }
        if (s->ptr == e)
            break;
        if ('?' == *s->ptr) {
            char *p = s->ptr++;
            --s->len;
            return p;
        }
        ++s->ptr;
        --s->len;
    }
    return NULL;
}

static int prepare_param (mdbres_t *res, int col, mdbfld_t *fld, strptr_t *str) {
    char *p = find_next_pholder(str), buf [64], *s;
    int pos = (intptr_t)p - (intptr_t)str->ptr;
    int len;
    if (!p) {
        dbcli_code = res->status = -1;
        *dbcli_state = 0;
        strcpy(dbcli_msg, "Not enough parameters");
        return -1;
    }
    if (!fld->ptr) {
        if (-1 == strepl(&res->query, p, 1, CONST_STR_LEN("NULL"))) {
            dbcli_code = res->status = -1;
            strcpy(dbcli_msg, "Parameter error");
            return -1;
        }
        str->len += sizeof("NULL") - 2;
        if (ERANGE == errno)
            str->ptr = res->query->ptr + pos + sizeof("NULL") - 2;
        return 0;
    }
    if (0 == fld->len)
        fld->len = strlen(fld->ptr);
    s = strndup(fld->ptr, fld->len);
    fld->ptr = s;
    switch (fld->type) {
        case MDB_TYPE_STR:
            len = fld->len + 4;
            s = malloc(len);
            len = snprintf(s, len, "'%s'", fld->ptr);
            if (-1 == strepl(&res->query, p, 1, s, len)) {
                free(fld->ptr);
                free(s);
                dbcli_code = res->status = -1;
                *dbcli_state = 0;
                strcpy(dbcli_msg, "Parameter error");
                return -1;
            }
            free(s);
            str->len += len - 1;
            if (ERANGE == errno)
                str->ptr = res->query->ptr + pos + len - 1;
            break;
        case MDB_TYPE_DATE:
            len = snprintf(buf, sizeof buf, "DATE '%s'", fld->ptr);
            if (-1 == strepl(&res->query, p, 1, buf, len)) {
                free(fld->ptr);
                dbcli_code = res->status = -1;
                *dbcli_state = 0;
                strcpy(dbcli_msg, "Parameter error");
                return -1;
            }
            str->len += len - 1;
            if (ERANGE == errno)
                str->ptr = res->query->ptr + pos + len - 1;
            break;
        case MDB_TYPE_TIME:
            len = snprintf(buf, sizeof buf, "TIME '%s'", fld->ptr);
            if (-1 == strepl(&res->query, p, 1, buf, len)) {
                free(fld->ptr);
                dbcli_code = res->status = -1;
                *dbcli_state = 0;
                strcpy(dbcli_msg, "Parameter error");
                return -1;
            }
            str->len += len - 1;
            if (ERANGE == errno)
                str->ptr = res->query->ptr + pos + len - 1;
            break;
        case MDB_TYPE_TIMESTAMP:
            len = snprintf(buf, sizeof buf, "DATETIME '%s'", fld->ptr);
            if (-1 == strepl(&res->query, p, 1, buf, len)) {
                free(fld->ptr);
                dbcli_code = res->status = -1;
                *dbcli_state = 0;
                strcpy(dbcli_msg, "Parameter error");
                return -1;
            }
            str->len += len - 1;
            if (ERANGE == errno)
                str->ptr = res->query->ptr + pos + len - 1;
            break;
        default:
            if (-1 == strepl(&res->query, p, 1, fld->ptr, fld->len)) {
                free(fld->ptr);
                dbcli_code = res->status = -1;
                *dbcli_state = 0;
                strcpy(dbcli_msg, "Parameter error");
                return -1;
            }
            str->len += fld->len - 1;
            if (ERANGE == errno)
                str->ptr = res->query->ptr + pos + fld->len - 1;
            break;
    }
    free(fld->ptr);
    return 0;
}

static int prepare_param_vals (mdbres_t *res, mdbfld_t *arg, va_list ap) {
    int col = 0;
    strptr_t str = { .ptr = res->query->ptr, .len = res->query->len };
    if (-1 == prepare_param(res, col++, arg, &str))
        return -1;
    while (1) {
        mdbfld_t fld = va_arg(ap, mdbfld_t);
        if (fld.is_end) {
            if (col != res->nparam) {
                dbcli_code = res->status = -1;
                *dbcli_state = 0;
                strcpy(dbcli_msg, "Parameter count error");
                return -1;
            }
            break;
        }
        if (-1 == prepare_param(res, col++, &fld, &str))
            return -1;
    }
    return 0;
}

mdbres_t *mdb_prepare (mdbconn_t *conn, const char *sql, size_t sql_len) {
    mdbres_t *res = calloc(1, sizeof(mdbres_t));
    if (0 == sql_len)
        sql_len = strlen(sql);
    res->conn = conn;
    res->sql_len = sql_len;
    res->sql = strndup(sql, sql_len);
    res->nparam = get_param_count(res->sql, res->sql_len);
    return res;
}

static int mdb_execp_va (mdbres_t *res, mdbfld_t *arg, va_list ap) {
    res->status = 0;
    res->rows = 0;
    if (!res || !res->sql) {
        dbcli_code = res->status = -1;
        *dbcli_state = 0;
        strcpy(dbcli_msg, "Statement is not prepared");
        return -1;
    }
    if (res->query)
        free(res->query);
    res->query = mkstr(res->sql, res->sql_len, 64);
    if (res->nparam > 0 && -1 == prepare_param_vals(res, arg, ap))
        return -1;
    if (!(res->stmt = mapi_query(res->conn->db, res->query->ptr)) || MOK != mapi_error(res->conn->db)) {
        dbcli_code = res->status = -1;
        strcpy(dbcli_state, mapi_result_errorcode(res->stmt));
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", mapi_result_error(res->stmt));
        return -1;
    }
    if (Q_UPDATE == (res->qtype = mapi_get_querytype(res->stmt)))
        res->rows = mapi_rows_affected(res->stmt);
    return 0;
}

int mdb_execp (mdbres_t *res, mdbfld_t arg, ...) {
    int rc;
    va_list ap;
    va_start(ap, arg);
    rc = mdb_execp_va(res, &arg, ap);
    va_end(ap);
    return rc;
}

mdbres_t *mdb_exec (mdbconn_t *conn, const char *sql, size_t sql_len, mdbfld_t arg, ...) {
    va_list ap;
    mdbres_t *res = mdb_prepare(conn, sql, sql_len);
    va_start(ap, arg);
    mdb_execp_va(res, &arg, ap);
    va_end(ap);
    return res;
}

long long int mdb_exec_sql (mdbconn_t *conn, const char *sql) {
    MapiHdl stmt = mapi_query(conn->db, sql);
    long long int rows = 0;
    if (!stmt)
        return -1;
    if (MOK != mapi_error(conn->db)) {
        mapi_close_handle(stmt);
        return -1;
    }
    if (Q_UPDATE == mapi_get_querytype(stmt))
        rows = mapi_rows_affected(stmt);
    mapi_close_handle(stmt);
    return rows;
}

void mdb_close (mdbres_t *res) {
    if (res->stmt)
        mapi_close_handle(res->stmt);
    if (res->sql)
        free(res->sql);
    if (res->query)
        free(res->query);
    free(res);
}

//******************************************************************************
// Get data
//******************************************************************************
char *mdb_get (mdbres_t *res, int col) {
    char *str;
    if (!(str = mapi_fetch_field(res->stmt, col)) && mapi_error(res->conn->db)) {
        dbcli_code = res->status = -1;
        strcpy(dbcli_state, mapi_result_errorcode(res->stmt));
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", mapi_result_error(res->stmt));
    }
    return str;
}

//******************************************************************************
// Connection pool
//******************************************************************************
dbpool_t *mdb_pool_create (const char *conn_info) {
    dbpool_t *pool = dbpool_create();
    dbpool_setopt(pool, DBPOOL_CONNECT, (dbpool_connect_h)mdb_connect);
    dbpool_setopt(pool, DBPOOL_DISCONNECT, (dbpool_h)mdb_disconnect);
    dbpool_setopt(pool, DBPOOL_CONNECT, conn_info);
    return pool;
}
