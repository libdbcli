/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mysql/mysql.h"

//******************************************************************************
// Connection
//******************************************************************************
static myconn_t *maria_connect_intr (const char *host, const char *dbname, const char *user, const char *pass) {
    myconn_t *conn = calloc(1, sizeof(myconn_t));
    mysql_init(&conn->db);
    if (!mysql_real_connect(&conn->db, host, user, pass, dbname, 0, NULL, 0)) {
        dbcli_code = mysql_errno(&conn->db);
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", (char*)mysql_error(&conn->db));
    }
    return conn;
}

myconn_t *maria_connect (const char *conn_info) {
    myconn_t *conn = calloc(1, sizeof(myconn_t));
    conn_info_t info;
    if (-1 == parse_conn_info(conn_info, &info)) {
        clear_conn_info(&info);
        dbcli_code = conn->status = -1;
        strcpy(dbcli_msg, "Connection parameter(s) error");
        return conn;
    }
    conn = maria_connect_intr(info.s_host, info.s_dbname, info.s_user, info.s_pass);
    clear_conn_info(&info);
    return conn;
}

void maria_disconnect (myconn_t *conn) {
    mysql_close(&conn->db);
    free(conn);
}

//******************************************************************************
// Get data
//******************************************************************************
int maria_is_null (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *bind->is_null;
}

char maria_get_tiny (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *((int8_t*)bind->buffer);
}

short int maria_get_smallint (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *((int16_t*)bind->buffer);
}

int maria_get_int (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *((int32_t*)bind->buffer);
}

long int maria_get_bigint (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *((int64_t*)bind->buffer);
}

float maria_get_float (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *((float*)bind->buffer);
}

double maria_get_double (myres_t *res, int col) {
    MYSQL_BIND *bind = &res->result[col];
    return *((double*)bind->buffer);
}

strptr_t maria_get_str (myres_t *res, int col, int n) {
    MYSQL_BIND *bind = &res->result[col];
    strptr_t str = {
        .ptr = n <= 0 ? bind->buffer : strndup(bind->buffer,
        *bind->length), .len = *bind->length
    };
    return str;
}

//******************************************************************************
// Statements
//******************************************************************************
static void maria_reset (myres_t *res) {
    if (res->res) mysql_free_result(res->res);
    if (res->params) free(res->params);
    if (res->result) {
        for (int i = 0; i < res->nflds; ++i)
            free(res->result[i].buffer);
        free(res->result);
    }
    if (res->param_nulls) free(res->param_nulls);
    if (res->nulls) free(res->nulls);
    if (res->errors) free(res->errors);
    if (res->lengths) free(res->lengths);
    if (res->param_lengths) free(res->param_lengths);
}

void maria_res_close (myres_t *res) {
    if (res->stmt) mysql_stmt_close(res->stmt);
    maria_reset(res);
    free(res);
}

myres_t *maria_prepare (myconn_t *conn, const char *sql, size_t sql_len) {
    myres_t *res = calloc(1, sizeof(myres_t));
    res->stmt = mysql_stmt_init(&conn->db);
    if (0 == (res->status = mysql_stmt_prepare(res->stmt, sql, sql_len > 0 ? sql_len : strlen(sql)))) {
        if (0 < (res->nparams = mysql_stmt_param_count(res->stmt))) {
            res->params = calloc(res->nparams, sizeof(MYSQL_BIND));
            res->param_nulls = calloc(res->nparams, sizeof(my_bool));
            res->param_lengths = calloc(res->nparams, sizeof(unsigned long));
        }
    } else {
        dbcli_code = mysql_stmt_errno(res->stmt);
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", mysql_stmt_error(res->stmt));
    }
    return res;
}

static int set_param (myres_t *res, int index, myfld_t *fld) {
    MYSQL_BIND *bind = &res->params[index];
    bind->is_null = &res->param_nulls[index];
    if (0 == (res->param_nulls[index] = fld->is_null)) {
        bind->buffer_type = fld->type;
        switch (fld->type) {
            case MYSQL_TYPE_STRING:
            case MYSQL_TYPE_VAR_STRING:
                bind->buffer = fld->data.s.ptr;
                bind->buffer_length = res->param_lengths[index] = fld->data.s.len;
                bind->length = &res->param_lengths[index];
                break;
            default:
                bind->buffer = (char*)(&fld->data);
                break;
        }
    }
    return 0;
}

static int set_params (myres_t *res, myfld_t first, va_list ap) {
    int rc = 0, n = 0;
    if (!first.type) {
        memset(res->param_nulls, 0, res->nparams * sizeof(my_bool));
        set_param(res, n++, &first);
        while (n < res->nparams) {
            myfld_t fld = va_arg(ap, myfld_t);
            if (0 == fld.type)
                break;
            if (0 != (rc = set_param(res, n++, &fld))) {
                dbcli_code = mysql_stmt_errno(res->stmt);
                snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", mysql_stmt_error(res->stmt));
                return rc;
            }
        }
    }
    if (n < res->nparams) {
        dbcli_code = -1;
        strcpy(dbcli_msg, "not enough parameters for query");
        rc = res->status = -1;
        return rc;
    }
    return mysql_stmt_bind_param(res->stmt, res->params);
}

static int bind_result (myres_t *res, int is_reuse) {
    if (!is_reuse)
    for (int i = 0; i < res->nflds; ++i) {
        MYSQL_FIELD *fld = mysql_fetch_field(res->res);
        MYSQL_BIND *col = &res->result[i];
        col->buffer_type = fld->type;
        col->buffer_length = fld->length;
        col->buffer = malloc(col->buffer_length + 4);
        col->is_null = &res->nulls[i];
        col->length = &res->lengths[i];
        col->error = &res->errors[i];
    }
    if (0 == (res->status = mysql_stmt_bind_result(res->stmt, res->result)))
        res->status = mysql_stmt_store_result(res->stmt);
    if (0 != res->status) {
        dbcli_code = mysql_stmt_errno(res->stmt);
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", mysql_stmt_error(res->stmt));
    }
    return res->status;
}

static void maria_execva (myres_t *res, myfld_t first, va_list ap) {
    int is_reuse = 0;
    if (res->res) {
        mysql_free_result(res->res);
        res->res = mysql_stmt_result_metadata(res->stmt);
        is_reuse = 1;
    } else
    if ((res->res = mysql_stmt_result_metadata(res->stmt))) {
        res->nflds = mysql_num_fields(res->res);
        res->result = malloc(res->nflds * sizeof(MYSQL_BIND));
        res->nulls = malloc(res->nflds * sizeof(my_bool));
        res->errors = malloc(res->nflds * sizeof(my_bool));
        res->lengths = malloc(res->nflds * sizeof(unsigned long));
    }
    if (res->res) {
        if (0 == (res->status = set_params(res, first, ap)) && 0 == (res->status = mysql_stmt_execute(res->stmt)))
            res->status = bind_result(res, is_reuse);
    } else
        res->status = mysql_stmt_execute(res->stmt);
    if (0 != res->status) {
        dbcli_code = mysql_stmt_errno(res->stmt);
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", mysql_stmt_error(res->stmt));
    }
}

myres_t *maria_exec (myconn_t *conn, const char *sql, size_t sql_len, myfld_t arg, ...) {
    myres_t *res = maria_prepare(conn, sql, sql_len);
    if (0 == res->status) {
        va_list ap;
        va_start(ap, arg);
        maria_execva(res, arg, ap);
        va_end(ap);
    }
    return res;
}

int maria_pexec (myres_t *res, myfld_t arg, ...) {
    va_list ap;
    va_start(ap, arg);
    maria_execva(res, arg, ap);
    va_end(ap);
    return res->status;
}

int maria_exec_sql (myconn_t *conn, const char *sql, size_t sql_len) {
    myres_t *res = maria_prepare(conn, sql, sql_len);
    int rc;
    if (0 == (rc = res->status))
        rc = mysql_stmt_execute(res->stmt);
    maria_res_close(res);
    return rc;
}

int maria_fetch (myres_t *res) {
    res->status = mysql_stmt_fetch(res->stmt);
    return res->status == MYSQL_NO_DATA ? 0 : 1;
}

//******************************************************************************
// Connection pool
//******************************************************************************
dbpool_t *maria_pool_create (const char *conn_info) {
    dbpool_t *pool = dbpool_create();
    dbpool_setopt(pool, DBPOOL_CONNECT, (dbpool_connect_h)maria_connect);
    dbpool_setopt(pool, DBPOOL_DISCONNECT, (dbpool_h)maria_disconnect);
    dbpool_setopt(pool, DBPOOL_CONNECT, conn_info);
    return pool;
}
