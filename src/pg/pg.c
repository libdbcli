/*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "pg/pg.h"

//******************************************************************************
// Data conversion
//******************************************************************************
#define MONTHS_PER_YEAR 12
#define POSTGRES_EPOCH_JDATE 2451545
#define INT64CONST(x)  (x##L)
#define USECS_PER_DAY INT64CONST(86400000000)
#define USECS_PER_HOUR INT64CONST(3600000000)
#define USECS_PER_MINUTE INT64CONST(60000000)
#define USECS_PER_SEC INT64CONST(1000000)
#define TMODULO(t,q,u) \
do { \
    (q) = ((t) / (u)); \
    if ((q) != 0) (t) -= ((q) * (u)); \
} while(0)
void j2date(int jd, int *year, int *month, int *day) {
    unsigned int julian;
    unsigned int quad;
    unsigned int extra;
    int y;
    julian = jd;
    julian += 32044;
    quad = julian / 146097;
    extra = (julian - quad * 146097) * 4 + 3;
    julian += 60 + quad * 3 + extra / 146097;
    quad = julian / 1461;
    julian -= quad * 1461;
    y = julian * 4 / 1461;
    julian = ((y != 0) ? ((julian + 305) % 365) : ((julian + 306) % 366)) + 123;
    y += quad * 4;
    *year = y - 4800;
    quad = julian * 2141 / 65536;
    *day = julian - 7834 * quad / 256;
    *month = (quad + 10) % MONTHS_PER_YEAR + 1;
}

static void dt2time(int64_t jd, int *hour, int *min, int *sec) {
    int64_t time;
    time = jd;
    *hour = time / USECS_PER_HOUR;
    time -= (*hour) * USECS_PER_HOUR;
    *min = time / USECS_PER_MINUTE;
    time -= (*min) * USECS_PER_MINUTE;
    *sec = time / USECS_PER_SEC;
}

int timestamp_to_tm (timestamp dt, struct tm *tm) {
    int64_t date, time = dt;
    memset(tm, 0, sizeof(struct tm));
    TMODULO(time, date, USECS_PER_DAY);
    if (time < INT64CONST(0)) {
        time += USECS_PER_DAY;
        date -= 1;
    }
    date += POSTGRES_EPOCH_JDATE;
    if (date < 0 || date > (int64_t)INT_MAX)
        return -1;
    j2date((int)date, &tm->tm_year, &tm->tm_mon, &tm->tm_mday);
    dt2time(time, &tm->tm_hour, &tm->tm_min, &tm->tm_sec);
    return 0;
}

struct tm *date_to_tm (date dt, struct tm *tm) {
    memset(tm, 0, sizeof(struct tm));
    j2date(dt + POSTGRES_EPOCH_JDATE, &tm->tm_year, &tm->tm_mon, &tm->tm_mday);
    return tm;
}

struct tm *interval_to_tm(interval_t *span, struct tm *tm) {
    int64 time;
    if (span->month != 0) {
        tm->tm_year = span->month / MONTHS_PER_YEAR;
        tm->tm_mon = span->month % MONTHS_PER_YEAR;
    } else {
        tm->tm_year = 0;
        tm->tm_mon = 0;
    }
    time = span->time;
    tm->tm_mday = time / USECS_PER_DAY;
    time -= tm->tm_mday * USECS_PER_DAY;
    tm->tm_hour = time / USECS_PER_HOUR;
    time -= tm->tm_hour * USECS_PER_HOUR;
    tm->tm_min = time / USECS_PER_MINUTE;
    time -= tm->tm_min * USECS_PER_MINUTE;
    tm->tm_sec = time / USECS_PER_SEC;
    return tm;
}

static double conv_double (double d) {
    union {
        double d;
        unsigned char bytes [8];
    } src, dst;
    src.d = d;
    dst.bytes[0] = src.bytes[7];
    dst.bytes[1] = src.bytes[6];
    dst.bytes[2] = src.bytes[5];
    dst.bytes[3] = src.bytes[4];
    dst.bytes[4] = src.bytes[3];
    dst.bytes[5] = src.bytes[2];
    dst.bytes[6] = src.bytes[1];
    dst.bytes[7] = src.bytes[0];
    return dst.d;
}

static float conv_float (float f) {
    union {
        float f;
        unsigned char bytes [4];
    } src, dst;
    src.f = f;
    dst.bytes[0] = src.bytes[3];
    dst.bytes[1] = src.bytes[2];
    dst.bytes[2] = src.bytes[1];
    dst.bytes[3] = src.bytes[0];
    return dst.f;
}

#define MAX_DIGITS (sizeof(mp_limb_t) / sizeof(int16_t))
static void set_digits_n (mpz_t x, int16_t *p_digits, int16_t *digits, int len, int weight) {
    int i = 1;
    mpz_t m;
    mpz_init_set_ui(m, 1);
    if (p_digits < digits)
        mpz_set_ui(x, 0);
    else
        mpz_set_ui(x, *p_digits);
    p_digits++;
    if (len < weight) {
        while (i < len) {
            if (p_digits < digits)
                mpz_mul_si(m, m, 10000);
            else
                mpz_set_ui(m, 10000);
            mpz_mul(x, x, m);
            mpz_add_ui(x, x, *p_digits++);
            i++;
        }
        while (i < weight) {
            mpz_mul_si(x, x, 10000);
            ++i;
        }
    } else {
        while (i < weight) {
            if (p_digits < digits)
                mpz_mul_si(m, m, 10000);
            else
                mpz_set_ui(m, 10000);
            mpz_mul(x, x, m);
            mpz_add_ui(x, x, *p_digits++);
            i++;
        }
    }
    mpz_clear(m);
}

static void set_digits_d (mpz_t x, int count) {
    int i = 0;
    while (i < count) {
        mpz_mul_si(x, x, 10000);
        i++;
    }
}

static numeric_t get_mpq (char *buf) {
    numeric_t res;
//    mpq_t res;
    mpq_init(res.num);
    int16_t len = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
    int16_t weight = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
    int16_t sign = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
    if (sign == NUMERIC_POS || sign == NUMERIC_NEG || sign == NUMERIC_NAN) {
        uint16_t dscale = res.dscale = be16toh(*(uint16_t*)buf); buf += sizeof(uint16_t);
        if ((dscale & NUMERIC_DSCALE_MASK) == dscale) {
            //mpq_init(fld->data.q);
            if (0 == len)
                return res;
            mpq_t x, y;
            mpq_ptr q = x;
            mpz_ptr n = &q->_mp_num, d = &q->_mp_den;
            int16_t *digits = malloc(len * sizeof(int16_t)), *p_digits = digits;
            for (int i = 0; i < len; ++i) {
                digits[i] = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
            }
            ++weight;
            mpz_init(n);
            if (weight > 0)
                set_digits_n(n, p_digits, digits, len, weight);
            mpz_init(d);
            mpz_set_si(d, 1);
            int cnt = len - weight;
            if (cnt <= 0)
                mpq_set(res.num, q);
            else {
                p_digits += weight;
                q = y;
                n = &q->_mp_num;
                d = &q->_mp_den;
                mpz_init(n);
                mpz_init_set_si(d, 1);
                set_digits_n(n, p_digits, digits, cnt, cnt);
                set_digits_d(d, cnt);
                mpq_add(res.num, x, y);
                mpq_clear(y);
            }
            if (sign == NUMERIC_NEG)
                mpq_neg(res.num, res.num);
            free(digits);
            mpq_clear(x);
        }
    }
    return res;
}

//******************************************************************************
// Database connection
//******************************************************************************
pgconn_t *pg_connect (const char *conn_info) {
    pgconn_t *conn = calloc(1, sizeof(pgconn_t));
    conn->db = PQconnectdb(conn_info);
    if (CONNECTION_OK != PQstatus(conn->db)) {
        conn->status = -1;
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", PQerrorMessage(conn->db));
    }
    return conn;
}

void pg_disconnect (pgconn_t *conn) {
    if (conn->db) PQfinish(conn->db);
    free(conn);
}

//******************************************************************************
// Get data
//******************************************************************************
int pg_isnull (pgres_t *res, int row, int col) {
    return PQgetisnull(res->stmt, row, col);
}

int pg_geti4 (pgres_t *res, int row, int col) {
    return be32toh(*((int32_t*)PQgetvalue(res->stmt, row, col)));
}

long int pg_geti8 (pgres_t *res, int row, int col) {
    return be64toh(*((int64_t*)PQgetvalue(res->stmt, row, col)));
}

float pg_getf4 (pgres_t *res, int row, int col) {
    return conv_float(*((float*)PQgetvalue(res->stmt, row, col)));
}

double pg_getf8 (pgres_t *res, int row, int col) {
    return conv_double(*((double*)PQgetvalue(res->stmt, row, col)));
}

strptr_t pg_getstr (pgres_t *res, int row, int col, int copy_data) {
    strptr_t str = { .len = PQgetlength(res->stmt, row, col) };
    if (PG_COPY == copy_data)
        str.ptr = strndup(PQgetvalue(res->stmt, row, col), str.len);
    else
        str.ptr = PQgetvalue(res->stmt, row, col);
    return str;
}

const char *pg_getstrptr (pgres_t *res, int row, int col) {
    return PQgetvalue(res->stmt, row, col);
}

int pg_getbool (pgres_t *res, int row, int col) {
    return *((int8_t*)PQgetvalue(res->stmt, row, col));
}

int pg_getdate (pgres_t *res, int row, int col) {
    return be32toh(*((date*)PQgetvalue(res->stmt, row, col))) - 1;
}

// TODO : now only 32 bit
unsigned int pg_getbit (pgres_t *res, int row, int col) {
    bit32_t *b = (bit32_t*)PQgetvalue(res->stmt, row, col);
    return be32toh(b->bit);
}

timestamp pg_gettm (pgres_t *res, int row, int col) {
#ifdef HAVE_INT64_TIMESTAMP
    return be64toh(*((timestamp*)PQgetvalue(res->stmt, row, col)));
#else
    return conv_double(*((timestamp*)PQgetvalue(res->stmt, row, col)));
#endif
}

interval_t pg_getintv (pgres_t *res, int row, int col) {
    interval_t *v = (interval_t*)PQgetvalue(res->stmt, row, col),
                r = { .time = be64toh(v->time), .day = be32toh(v->day), .month = be32toh(v->month) };
    return r;
}

typedef union {
    unsigned int l;
    unsigned char a [4];
} pg_inet4;
// TODO : now only ipv4
unsigned int pg_getinet (pgres_t *res, int row, int col) {
    unsigned char *v = (unsigned char*)PQgetvalue(res->stmt, row, col);
    if (2 == v[0] && 32 == v[1] && *(unsigned short*)(v+2) == 4) {
        pg_inet4 f;
        f.a[0] = v[7];
        f.a[1] = v[6];
        f.a[2] = v[5];
        f.a[3] = v[4];
        return be32toh(f.l);
    }
    return -1;
}

numeric_t pg_getnum (pgres_t *res, int row, int col) {
    return get_mpq(PQgetvalue(res->stmt, row, col));
}

unsigned char *pg_getuuid (pgres_t *res, int row, int col) {
    return (unsigned char*)PQgetvalue(res->stmt, row, col);
}

//******************************************************************************
// Set data
//******************************************************************************
void pg_set (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = params->lens[col] = params->fmts[col] = params->is_free[col] = 0;
    params->vals[col] = fld->is_null ? NULL : fld->data.s;
}

void pg_seti4 (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    fld->data.i4 = htobe32(fld->data.i4);
    params->vals[col] = (const char*)&fld->data.i4;
    params->lens[col] = sizeof(int32_t);
    params->fmts[col] = 1;
}

void pg_seti8 (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    fld->data.i8 = htobe64(fld->data.i8);
    params->vals[col] = (const char*)&fld->data.i8;
    params->lens[col] = sizeof(int64_t);
    params->fmts[col] = 1;
}

void pg_setf4 (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    fld->data.f4 = conv_float(fld->data.f4);
    params->vals[col] = (const char*)&fld->data.f4;
    params->lens[col] = sizeof(float);
    params->fmts[col] = 1;
}

void pg_setf8 (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    fld->data.f8 = conv_double(fld->data.f8);
    params->vals[col] = (const char*)&fld->data.f8;
    params->lens[col] = sizeof(double);
    params->fmts[col] = 1;
}

void pg_setstr (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    if (fld->len <= 0) {
        params->vals[col] = fld->data.s;
        params->is_free[col] = 0;
    } else {
        params->vals[col] = (const char*)strndup(fld->data.s, fld->len);
        params->is_free[col] = 1;
    }
    params->lens[col] = params->fmts[col] = 0;
}

void pg_setbytea (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    params->vals[col] = (const char*)fld->data.s;
    params->lens[col] = fld->len;
    params->fmts[col] = 1;
}

void pg_setbool (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    params->vals[col] = (const char*)&fld->data.b;
    params->lens[col] = sizeof(int8_t);
    params->fmts[col] = 1;
}

void pg_setdate (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    fld->data.d = htobe32(fld->data.d);
    params->vals[col] = (const char*)&fld->data.d;
    params->lens[col] = sizeof(date);
    params->fmts[col] = 1;
}

void pg_settimestamp (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    #ifdef HAVE_INT64_TIMESTAMP
    fld->data.tm = htobe64(fld->data.tm);
    #else
    fld->data.tm = conv_double(fld->data.tm);
    #endif
    params->vals[col] = (const char*)&fld->data.tm;
    params->lens[col] = sizeof(timestamp);
    params->fmts[col] = 1;
}

void pg_setuuid (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    params->vals[col] = (const char*)fld->data.uuid;
    params->lens[col] = sizeof(uuid_t);
    params->fmts[col] = 1;
}

// TODO : now only 32 bit
void pg_setbit (pgfld_t *fld, int col, pg_params_t *params) {
    params->types[col] = fld->type;
    params->is_free[col] = 0;
    fld->data.bit32.len = htobe32(fld->data.bit32.len);
    fld->data.bit32.bit = htobe32(fld->data.bit32.bit);
    params->vals[col] = (const char*)&fld->data.bit32;
    params->lens[col] = sizeof(bit32_t);
    params->fmts[col] = 1;
}

//******************************************************************************
// Statement
//******************************************************************************
static void pg_release (pgconn_t *conn, const char *name) {
    size_t len = strlen(name) + 16;
    char *sql = alloca(len);
    snprintf(sql, len, "deallocate %s", name);
    pg_exec_sql(conn, sql);
}

void pg_close (pgres_t *res) {
    if (res->name) {
        pg_release(res->conn, res->name);
        free((void*)res->name);
    }
    if (res->stmt_prepared)
        PQclear(res->stmt_prepared);
    if (res->stmt)
        PQclear(res->stmt);
    free(res);
}

static int init_param_args (pg_params_t *params, pgfld_t *arg) {
    memset(params, 0, sizeof(pg_params_t));
    if (arg->is_null || arg->setfld)
        params->params_len = 1;
    return params->params_len;
}

static void alloc_params (pg_params_t *params) {
    params->types = malloc(sizeof(Oid) * params->params_len);
    params->vals = malloc(sizeof(const char*) * params->params_len);
    params->lens = malloc(sizeof(int) * params->params_len);
    params->fmts = malloc(sizeof(int) * params->params_len);
    params->is_free = malloc(sizeof(unsigned char) * params->params_len);
}

static void prepare_param_args (pg_params_t *params, va_list ap) {
    while (1) {
        pgfld_t f = va_arg(ap, pgfld_t);
        if (!f.is_null && !f.setfld)
            break;
        ++params->params_len;
    }
    alloc_params(params);
}

static void set_param_args (pg_params_t *params, pgfld_t *arg, va_list ap) {
    int col = 0;
    arg->setfld(arg, col++, params);
    for (int i = 1; i < params->params_len; ++i) {
        pgfld_t f = va_arg(ap, pgfld_t);
        f.setfld(&f, col++, params);
    }
}

static void clear_params (pg_params_t *params) {
    if (params->params_len > 0) {
        free(params->fmts);
        free(params->lens);
        for (int i = 0; i < params->params_len; ++i)
            if (params->is_free[i])
                free((void*)params->vals[i]);
        free(params->vals);
        free(params->types);
        free(params->is_free);
    }
}

static void _get_result (pgres_t *res) {
    char *s = PQresultErrorField(res->stmt, PG_DIAG_SQLSTATE);
    if (s)
        strcpy(dbcli_state, s);
    switch (PQresultStatus(res->stmt)) {
        case PGRES_COMMAND_OK:
            res->nrows = strtol(PQcmdTuples(res->stmt), NULL, 0);
            break;
        case PGRES_TUPLES_OK:
        case PGRES_SINGLE_TUPLE:
            res->nrecs = PQntuples(res->stmt);
            res->nflds = PQnfields(res->stmt);
            break;
        default:
            res->status = -1;
            snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", PQresultErrorMessage(res->stmt));
            break;
    }
}

static pgres_t *exec_params (pgconn_t *conn, const char *sql, pg_params_t *params) {
    pgres_t *res = calloc(1, sizeof(pgres_t));
    res->stmt = PQexecParams(conn->db, sql, params->params_len, params->types, params->vals, params->lens, params->fmts, 1);
    _get_result(res);
    return res;
}

pgres_t *pg_exec (pgconn_t *conn, const char *sql, pgfld_t arg, ...) {
    pgres_t *res;
    pg_params_t params;
    if (init_param_args(&params, &arg) > 0) {
        va_list ap;
        va_start(ap, arg);
        prepare_param_args(&params, ap);
        va_end(ap);
        va_start(ap, arg);
        set_param_args(&params, &arg, ap);
        va_end(ap);
    }
    res = exec_params(conn, sql, &params);
    clear_params(&params);
    return res;
}

int pg_add_param (clist_t *params, pgfld_t arg) {
    clist_item_t *li;
    if (0 == arg.is_null && NULL == arg.setfld)
        return -1;
    if ((li = clst_add(params, &arg, sizeof(pgfld_t))))
        return 0;
    return -1;
}

int pg_add_param_type (clist_t *params, Oid arg) {
    clist_item_t *li;
    if (arg <= 0)
        return -1;
    if ((li = clst_add(params, &arg, sizeof(Oid))))
        return 0;
    return -1;
}

static void set_param_list (pg_params_t *params, clist_t *args) {
    int col = 0;
    params->params_len = args->len;
    alloc_params(params);
    CLST_FOREACH(args, pgfld_t, fld)
        fld->setfld(fld, col++, params);
    CLST_END(args)
}

pgres_t *pg_execl (pgconn_t *conn, const char *sql, clist_t *args) {
    pg_params_t params;
    pgres_t *res;
    memset(&params, 0, sizeof params);
    if (args && args->len > 0)
        set_param_list(&params, args);
    res = exec_params(conn, sql, &params);
    clear_params(&params);
    return res;
}

static pgres_t *_prepare (pgconn_t *conn, const char *name, const char *sql, int nparams, Oid *types) {
    PGresult *stmt = NULL;
    pgres_t *res = calloc(1, sizeof(pgres_t));
    if (!(stmt = PQprepare(conn->db, name, sql, nparams, types))) {
        res->status = -1;
        snprintf(dbcli_msg, DBCLI_MSG_LEN, "%s", strdup(PQerrorMessage(conn->db)));
        return res;
    }
    res->conn = conn;
    res->name = strdup(name);
    res->stmt_prepared = stmt;
    return res;
}

pgres_t *pg_prepare (pgconn_t *conn, const char *name, const char *sql, int arg, ...) {
    Oid *types = NULL;
    pgres_t *res;
    int nparams = arg > 0 ? 1 : 0;
    if (nparams > 0) {
        int type, col = 0;
        va_list ap;
        va_start(ap, arg);
        while (va_arg(ap, int) > 0)
            ++nparams;
        va_end(ap);
        types = malloc(nparams * sizeof(Oid));
        va_start(ap, arg);
        types[col++] = arg;
        while ((type = va_arg(ap, int)) > 0)
            types[col++] = type;
        va_end(ap);
    }
    res = _prepare(conn, name, sql, nparams, types);
    if (types)
        free(types);
    return res;
}

pgres_t *pg_preparel (pgconn_t *conn, const char *name, const char *sql, clist_t *args) {
    Oid *types = NULL;
    pgres_t *res;
    if (args && args->len > 0) {
        int col = 0;
        types = malloc(args->len * sizeof(Oid));
        CLST_FOREACH(args, Oid, type)
            types[col++] = *type;
        CLST_END(args)
    }
    res = _prepare(conn, name, sql, args->len, types);
    if (types)
        free(types);
    return res;
}

static void _exec_prepared_params (pgres_t *res, const char *name, pg_params_t *params) {
    res->stmt = PQexecPrepared(res->conn->db, res->name, params->params_len, params->vals, params->lens, params->fmts, 1);
    _get_result(res);
}

int pg_execp (pgres_t *res, pgfld_t arg, ...) {
    pg_params_t params;
    memset(&params, 0, sizeof(pg_params_t));
    if (init_param_args(&params, &arg) > 0) {
        va_list ap;
        va_start(ap, arg);
        prepare_param_args(&params, ap);
        va_end(ap);
        va_start(ap, arg);
        set_param_args(&params, &arg, ap);
        va_end(ap);
    }
    _exec_prepared_params(res, res->name, &params);
    clear_params(&params);
    return res->status;
}

int pg_execpl (pgres_t *res, clist_t *args) {
    pg_params_t params;
    memset(&params, 0, sizeof(pg_params_t));
    if (args && args->len > 0)
        set_param_list(&params, args);
    _exec_prepared_params(res, res->name, &params);
    clear_params(&params);
    return res->status;
}

int pg_exec_sql (pgconn_t *conn, const char *sql) {
    PGresult *stmt = PQexec(conn->db, sql);
    int status = PQresultStatus(stmt);
    PQclear(stmt);
    return PGRES_COMMAND_OK == status || PGRES_TUPLES_OK == status ? 0 : -1;
}

static char *tx_level [] = {
    "", "read committed", "repeatable read", "serializable", "read uncommitted"
};

static char *rw_mode [] = { "", "read write", "read only" };

int pg_start (pgconn_t *conn, int level, int mode) {
    char sql [64];
    if (level < 0 || level >= sizeof(tx_level)/sizeof(void*))
        level = PG_DEFAULT;
    if (mode < 0 || mode >= sizeof(rw_mode)/sizeof(void*))
        mode = PG_DEFAULT;
    snprintf(sql, sizeof sql, "start transaction %s %s", tx_level[level], rw_mode[mode]);
    return pg_exec_sql(conn, sql);
}

//******************************************************************************
// Connection pool
//******************************************************************************
dbpool_t *pg_pool_create (const char *conn_info) {
    dbpool_t *pool = dbpool_create();
    dbpool_setopt(pool, DBPOOL_CONNECT, (dbpool_connect_h)pg_connect);
    dbpool_setopt(pool, DBPOOL_DISCONNECT, (dbpool_h)pg_disconnect);
    dbpool_setopt(pool, DBPOOL_CONNECT, conn_info);
    return pool;
}
