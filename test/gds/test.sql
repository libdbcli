connect "localhost:/var/lib/firebird/3.0/data/test.fdb" user "SYSDBA" password "masterkey";

create table t01 (
    i4 integer not null,
    i8 bigint not null,
    s varchar(50) not null,
    ts timestamp default 'now' not null,
    primary key (i4)
);

create generator i4_gen;

set term !! ;
create trigger set_i4_gen for t01
before insert as
begin
    if (new.i4 is null) then
        new.i4 = gen_id(i4_gen, 1);
end !!
set term ; !!

grant all privileges on t01 to public;

insert into t01 (i8, s) values (5, 'aaaa');
