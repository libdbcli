#include "gds/gds.h"

void test_00 (gdsconn_t *conn, gdstr_t *tr) {
    gds_exec_sql(conn, tr, "delete from t01", 0);
}

void test_01 (gdsconn_t *conn, gdstr_t *tr) {
    gdsres_t *res = gds_prepare(conn, tr, "insert into t01 (i8, s) values (?, ?)", 0);
    if (0 == res->status) {
        gds_exec(res, tr, GDS_I8(10), GDS_STR("asdf",0), GDS_END);
        gds_exec(res, tr, GDS_I8(15), GDS_STR("hgfd",0), GDS_END);
        gds_exec(res, tr, GDS_I8(20), GDS_STR("nbvc",0), GDS_END);
    }
    gds_close(res);
}

void test_02 (gdsconn_t *conn, gdstr_t *tr) {
    gdsres_t *res = gds_prepare(conn, tr, "select i4, i8, s, ts from t01", 0);
    if (0 == res->status && gds_exec(res, tr, GDS_END) > 0) {
        while (1 == gds_fetch(res)) {
            printf("i4: %d\n", gds_get_i4(res, 0));
            printf("i8: %ld\n", gds_get_i8(res, 1));
            printf("s: %s\n", gds_get_str(res, 2));
            ISC_TIMESTAMP ts = gds_get_timestamp(res, 3);
            struct tm tm;
            char buf [50];
            isc_decode_timestamp(&ts, &tm);
            strftime(buf, sizeof buf, "%a, %d %b %Y %H:%M:%S GMT ", &tm);
            printf("ts: %s\n", buf);
            printf("\n");
        }
    }
    gds_close(res);
}

int main (int argc, const char *argv[]) {
    gdsconn_t *conn = gds_connect("host=localhost dbname=/var/lib/firebird/3.0/data/test.fdb user=SYSDBA pass=masterkey");
    if (0 == conn->status) {
        gdstr_t tran = 0;
        isc_tr_handle *tr = NULL;
        if (argc > 1 && 0 == strcmp(argv[1], "tr")) {
            tran = gds_start(GDS_TRAN_DEFAULT, conn, NULL);
            if (-1 == conn->status) {
                gds_disconnect(conn);
                return 1;
            }
            tr = &tran;
        }
        test_00(conn, tr);
        test_01(conn, tr);
        test_02(conn, tr);
        if (tr)
            gds_commit(tr);
    }
    gds_disconnect(conn);
    return 0;
}
