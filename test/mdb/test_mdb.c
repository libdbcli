#include "mdb/mdb.h"

void test_01 (mdbconn_t *conn) {
    mdbres_t *res = mdb_prepare(conn, "insert into t01 values (?,?,?,?,?,?,?)", 0);
    mdb_execp(res, MDB("12",0), MDB("467",0), MDB("96",0), MDB("321",0), MDB("21.67",0), MDB("689.981",0), MDB_STR("good luck 1",0), MDB_END);
    printf("%ld rows affected\n", res->rows);
    mdb_execp(res, MDB("13",0), MDB("468",0), MDB("97",0), MDB("322",0), MDB("22.68",0), MDB("690.982",0), MDB_STR("good luck 2",0), MDB_END);
    printf("%ld rows affected\n", res->rows);
    mdb_execp(res, MDB("14",0), MDB("469",0), MDB("98",0), MDB("323",0), MDB("23.69",0), MDB("691.983",0), MDB_STR("good luck 3",0), MDB_END);
    printf("%ld rows affected\n", res->rows);
    mdb_close(res);
}

void test_02 (mdbconn_t *conn) {
    int flds;
    mdbres_t *res = mdb_exec(conn, "select * from t01 where i1 = ? and v = ?", 0, MDB("12",0), MDB_STR("good luck 1",0), MDB_END);
    while ((flds = mdb_fetch(res)) > 0) {
        for (int i = 0; i < flds; ++i)
            printf("%s ", mdb_get(res, i));
        printf("\n");
    }
    mdb_close(res);
}

void test_03 (mdbconn_t *conn) {
    long long int rows = mdb_exec_sql(conn, "delete from t01");
    printf("%ld rows affected\n", rows);
}

int main () {
    mdbconn_t *conn = mdb_connect("dbname=test user=test pass=test");
    if (0 == conn->status) {
        test_01(conn);
        test_02(conn);
        test_03(conn);
    }
    mdb_disconnect(conn);
    return 0;
}
