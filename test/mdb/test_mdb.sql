start transaction;

create table t01 (
    i1          tinyint,
    i2          smallint,
    i4          integer,
    i8          bigint,
    f4          real,
    f8          double precision,
    v           varchar(50)
);

commit;