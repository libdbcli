#include <stdio.h>
#include <uuid/uuid.h>
#include <libex/file.h>
#include "pg/pg.h"

void test_00 (pgconn_t *conn) {
    pgres_t *res = pg_exec(conn, "select * from test", PG_END);
    if (0 == res->status) {
        for (int i = 0; i < res->nrecs; ++i) {
            const char *a = pg_getstrptr(res, i, 0),
                       *b = pg_getstrptr(res, i, 1);
            int l1 = strlen(a),
                l2 = strlen(b);
            printf("x\n");
        }
    }
    pg_close(res);
}

void test_01 (pgconn_t *conn) {
    pgres_t *res = pg_exec(conn, "select * from t01 where i=$1", PG_I4(12), PG_END);
    if (0 == res->status) {
        for (int i = 0; i < res->nrecs; ++i) {
            printf("%s\n", pg_getstrptr(res, i, 4));
        }
    }
    pg_close(res);
}

void test_02 (pgconn_t *conn) {
    clist_t *params = clst_alloc();
    pg_add_param(params, PG_I4(12));
    pgres_t *res = pg_execl(conn, "select * from t01 where i=$1", params);
    if (0 == res->status) {
        for (int i = 0; i < res->nrecs; ++i)
            printf("%s\n", pg_getstrptr(res, i, 4));
    }
    pg_close(res);
    clst_free(params);
}

void test_03 (pgconn_t *conn) {
    pgres_t *res = pg_prepare(conn, "x", "select * from t01 where i=$1", OID_INT4, 0);
    if (0 == res->status && 0 == pg_execp(res, PG_I4(12), PG_END)) {
        for (int i = 0; i < res->nrecs; ++i)
            printf("%s\n", pg_getstrptr(res, i, 4));
    }
    pg_close(res);
}

void test_04 (pgconn_t *conn) {
    clist_t *types = clst_alloc();
    pgres_t *res = NULL;
    pg_add_param_type(types, OID_INT4);
    res = pg_preparel(conn, "x", "select * from t01 where i=$1", types);
    if (0 == res->status) {
        clist_t *params = clst_alloc();
        pg_add_param(params, PG_I4(12));
        if (0 == pg_execpl(res, params)) {
            for (int i = 0; i < res->nrecs; ++i)
                printf("%s\n", pg_getstrptr(res, i, 4));
        }
        clst_free(params);
    }
    pg_close(res);
    clst_free(types);
}

void test_05 (pgconn_t *conn) {
    pgres_t *res = pg_exec(conn, "select * from t01 where v=$1", PG_STR("good luck!",0), PG_END);
    if (0 == res->status) {
        for (int i = 0; i < res->nrecs; ++i) {
            printf("%d\n", pg_geti4(res, i, 0));
        }
    }
    pg_close(res);
}

void test_06 (pgconn_t *conn) {
    strptr_t s = CONST_STR_INIT("good luck!");
    pgres_t *res = pg_exec(conn, "select * from t01 where v=$1", PG_STR(s.ptr, s.len), PG_END);
    if (0 == res->status) {
        for (int i = 0; i < res->nrecs; ++i) {
            printf("%d\n", pg_geti4(res, i, 0));
        }
    }
    pg_close(res);
}

int main () {
    printf("%d\n", sizeof(pgfld_t));
    pgconn_t *conn = pg_connect("postgres://user:pass@localhost/test");
    if (0 == conn->status) {
        test_00(conn);
/*        test_01(conn);
        test_02(conn);
        test_03(conn);
        test_04(conn);
        test_05(conn);
        test_06(conn);*/
    }
    pg_disconnect(conn);
    return 0;
}
