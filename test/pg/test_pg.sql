-- database test
create table t01 (
      i       int not null,
      ii      bigint,
      f       float,
      d       double precision,
      v       varchar(50),
      b       bool,
      dt      date,
      tm      timestamp,
      bi      bit(32),
      m       money
);
insert into t01 values (12, 123, 2.34, 4.56, 'good luck!', 't', '1985-01-04', '1981-02-05 13:34', X'00000025', 12.50::numeric);
