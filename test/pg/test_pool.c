#include "pg/pg.h"

int main () {
    dbpool_t *pool = pg_pool_create("dbname=test");
    if (0 == dbpool_start(pool)) {
        pgconn_t *c1 = pg_pool_get(pool),
                 *c2 = pg_pool_get(pool),
                 *c3 = pg_pool_get(pool);
        if (c1)
            pg_pool_release(c1);
        if (c2)
            pg_pool_release(c2);
        if (c3)
            pg_pool_release(c3);
    }
    dbpool_free(pool);
    return 0;
}
